#Project
cmake_minimum_required(VERSION 2.4)
project(stereoflow)

# Include directories
include_directories(include src/revlib/include src/revlib/external/libpng src/revlib/external/zlib src/common)

# Library directories
#link_directories(src/revlib)

# Flags
set(CMAKE_CXX_FLAGS_RELEASE "-Wall -O3 -msse4.2")
set(CMAKE_BUILD_TYPE Release)

# Output directories
set(LIBRARY_OUTPUT_PATH src/revlib)
set(EXECUTABLE_OUTPUT_PATH bin)

# RevLib
file(GLOB REV_SRC_FILES "src/revlib/src/*.cpp" "src/revlib/external/libpng/*.c" "src/revlib/external/zlib/*.c")
add_library(revlib ${REV_SRC_FILES})

# SGM-Stereo
file(GLOB SGMSTEREO_SRC_FILES "src/stereo/sgmstereo/*.cpp")
add_executable(sgmstereo ${SGMSTEREO_SRC_FILES})
target_link_libraries(sgmstereo revlib)

# StereoSLIC
file(GLOB STEREOSLIC_SRC_FILES "src/stereo/stereoslic/*.cpp")
add_executable(stereoslic ${STEREOSLIC_SRC_FILES})
target_link_libraries(stereoslic revlib)

# SiftFund
file(GLOB SIFTFUND_SRC_FILES "src/flow/siftfund/*.cpp")
add_executable(siftfund ${SIFTFUND_SRC_FILES})
target_link_libraries(siftfund revlib)

# SGM-Flow
file(GLOB SGMFLOW_SRC_FILES "src/flow/sgmflow/*.cpp" "src/common/*.cpp")
add_executable(sgmflow ${SGMFLOW_SRC_FILES})
target_link_libraries(sgmflow revlib)

# MotionSLIC
file(GLOB MOTIONSLIC_SRC_FILES "src/flow/motionslic/*.cpp" "src/common/*.cpp")
add_executable(motionslic ${MOTIONSLIC_SRC_FILES})
target_link_libraries(motionslic revlib)

# EstAlpha
file(GLOB ESTALPHA_SRC_FILES "src/stereoflow/estalpha/*.cpp" "src/common/*.cpp")
add_executable(estalpha ${ESTALPHA_SRC_FILES})
target_link_libraries(estalpha revlib)

# SGM-StereoFlow
file(GLOB SGMSTEREOFLOW_SRC_FILES "src/stereoflow/sgmstereoflow/*.cpp" "src/common/*.cpp")
add_executable(sgmstereoflow ${SGMSTEREOFLOW_SRC_FILES})
target_link_libraries(sgmstereoflow revlib)

# Slanted plane smoothing
file(GLOB SMOOTHFIT_SRC_FILES "src/stereoflow/smoothfit/*.cpp" "src/common/*.cpp")
add_executable(smoothfit ${SMOOTHFIT_SRC_FILES})
target_link_libraries(smoothfit revlib)
