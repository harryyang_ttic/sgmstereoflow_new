#include <revlib.h>

rev::Image<unsigned short> interpolateDisparityImage(const rev::Image<unsigned short>& disparityImage);
rev::Image<double> interpolateDisparityImage(const rev::Image<double>& disparityImage);
