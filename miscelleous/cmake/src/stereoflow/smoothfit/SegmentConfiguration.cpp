#include "SegmentConfiguration.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <algorithm>
#include "StereoException.h"

void SegmentConfiguration::build(const rev::Image<unsigned short>& segmentIndexImage,
                                 const std::vector<SupportPoint>& supportPoints,
                                 const int windowRadius,
                                 const bool associateBoundary)
{
    // Set segment index image
    setSegmentIndexImage(segmentIndexImage);
    // Build segment configuration
    buildSegmentRelationship();
    // Append support points
    setDisparityCandidateMax(supportPoints);
    appendSupportPoint(supportPoints, windowRadius, associateBoundary);
}

void SegmentConfiguration::buildWithoutSupportPoint(const rev::Image<unsigned short>& segmentIndexImage) {
    supportPointDisparityCandidateMax_ = 0;
    // Set segment index image
    setSegmentIndexImage(segmentIndexImage);
    // Build segment configuration
    buildSegmentRelationship();
}

void SegmentConfiguration::appendSupportPointToSegmentData(const std::vector<SupportPoint>& supportPoints,
                                                           const int windowWidth,
                                                           const bool associateBoundary)
{
    setDisparityCandidateMax(supportPoints);
    appendSupportPoint(supportPoints, windowWidth, associateBoundary);
}

Segment SegmentConfiguration::segment(const int index) const {
    if (index < 0 || index >= static_cast<int>(segments_.size())) {
        throw StereoException("SegmentConfiguration::segment", "index is not valid");
    }
    
    return segments_[index];
}

SegmentBoundary SegmentConfiguration::boundary(const int index) const {
    if (index < 0 || index >= static_cast<int>(boundaries_.size())) {
        throw StereoException("SegmentConfiguration::boundary", "index is not valid");
    }
    
    return boundaries_[index];
}

SegmentJunction SegmentConfiguration::junction(const int index) const {
    if (index < 0 || index >= static_cast<int>(junctions_.size())) {
        throw StereoException("SegmentConfiguration::junction", "index is not valid");
    }
    
    return junctions_[index];
}

SegmentCrossJunction SegmentConfiguration::crossJunction(const int index) const {
    if (index < 0 || index >= static_cast<int>(crossJunctions_.size())) {
        throw StereoException("SegmentConfiguration::crossJunction", "index is not valid");
    }
    
    return crossJunctions_[index];
}

void SegmentConfiguration::setSegmentIndexImage(const rev::Image<unsigned short>& segmentIndexImage) {
    // Check segment index image
	if (segmentIndexImage.channelTotal() != 1) {
		throw StereoException("SegmentConfiguration::setSegmentIndexImage", "segment index image is not single channel image");
	}
	
	segmentIndexImage_ = segmentIndexImage;
}

void SegmentConfiguration::buildSegmentRelationship() {
    // The number of segments
    int segmentTotal = countSegmentTotal();
    
    // Image size
    int width = segmentIndexImage_.width();
    int height = segmentIndexImage_.height();
    
    // Sums of pixel coordinates in segments
	std::vector<int> segmentXSums(segmentTotal);
	std::vector<int> segmentYSums(segmentTotal);
	std::vector<int> segmentPixelTotals(segmentTotal);
    for (int i = 0; i < segmentTotal; ++i) {
        segmentXSums[i] = 0;
        segmentYSums[i] = 0;
        segmentPixelTotals[i] = 0;
    }
    
    // Set size of segments
    segments_.resize(segmentTotal);
	
	// Initialize boundary index image
	boundaryIndexImage_.resize(width, height, 1);
	boundaryIndexImage_.setTo(-1);
	int boundaryWidth = 2;
    
    // Classify each pixel
    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            // Add pixel coordinates
            int pixelSegmentIndex = segmentIndexImage_(x, y);
            segmentXSums[pixelSegmentIndex] += x;
            segmentYSums[pixelSegmentIndex] += y;
            ++segmentPixelTotals[pixelSegmentIndex];
			segments_[pixelSegmentIndex].appendSegmentPixel(x, y);
			
            // Horizontal boundary
            if (isHorizontalBoundary(x, y)) {
                int firstSegmentIndex = segmentIndexImage_(x, y);
                int secondSegmentIndex = segmentIndexImage_(x+1, y);
                int boundaryIndex = appendBoundary(firstSegmentIndex, secondSegmentIndex);
                boundaries_[boundaryIndex].appendBoundaryPixel(x+0.5, y);
				setBoundaryIndexImageHorizontal(x, y, boundaryIndex, boundaryWidth);
            }
            // Vertical boundary
            if (isVerticalBoundary(x, y)) {
                int firstSegmentIndex = segmentIndexImage_(x, y);
                int secondSegmentIndex = segmentIndexImage_(x, y+1);
                int boundaryIndex = appendBoundary(firstSegmentIndex, secondSegmentIndex);
                boundaries_[boundaryIndex].appendBoundaryPixel(x, y+0.5);
				setBoundaryIndexImageVertical(x, y, boundaryIndex, boundaryWidth);
            }
            
            // Junction
			std::vector<int> junctionSegmentIndices;
            if (isJunction(x, y, junctionSegmentIndices)) {
                appendJunction(x, y, junctionSegmentIndices);
            }
        }
    }
    
    // Set centers and boundaries of segments
    for (int segmentIndex = 0; segmentIndex < segmentTotal; ++segmentIndex) {
        // Segment center
        double centerX = static_cast<double>(segmentXSums[segmentIndex])/segmentPixelTotals[segmentIndex];
        double centerY = static_cast<double>(segmentYSums[segmentIndex])/segmentPixelTotals[segmentIndex];
        segments_[segmentIndex].setCenter(centerX, centerY);
        
        // Boundary indices
        setBoundaryIndicesInSegment(segmentIndex);
    }
    
    // Set boundary indices to junctions
    setBoundaryIndicesInAllJunctions();
    setBoundaryIndicesInAllCrossJunctions();
}

void SegmentConfiguration::setDisparityCandidateMax(const std::vector<SupportPoint>& supportPoints) {
    int pointTotal = static_cast<int>(supportPoints.size());

    supportPointDisparityCandidateMax_ = 0;
    for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
        if (supportPoints[pointIndex].disparityTotal() > supportPointDisparityCandidateMax_) {
            supportPointDisparityCandidateMax_ = supportPoints[pointIndex].disparityTotal();
        }
    }
}

void SegmentConfiguration::appendSupportPoint(const std::vector<SupportPoint>& supportPoints,
                                              const int windowRadius,
                                              const bool associateBoundary)
{
    int pointTotal = static_cast<int>(supportPoints.size());
    for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
        int x = supportPoints[pointIndex].x();
        int y = supportPoints[pointIndex].y();
		
		std::vector<int> segmentIndicesWithinWindow = getSegmentIndicesWithinWindow(x, y, windowRadius);
        if (segmentIndicesWithinWindow.size() == 1) {
            // Interior point in segment
            segments_[segmentIndicesWithinWindow[0]].appendInteriorPoint(supportPoints[pointIndex]);
        } else if (segmentIndicesWithinWindow.size() == 2 || associateBoundary) {
            // Boundary point
            for (int boundaryIndex = 0; boundaryIndex < static_cast<int>(boundaries_.size()); ++boundaryIndex) {
                if (boundaries_[boundaryIndex].consistOf(segmentIndicesWithinWindow[0], segmentIndicesWithinWindow[1])) {
                    boundaries_[boundaryIndex].appendBoundaryPoint(supportPoints[pointIndex]);
                    break;
                }
            }
        } else if (segmentIndicesWithinWindow.size() == 3) {
            // Junction point
            for (int junctionIndex = 0; junctionIndex < static_cast<int>(junctions_.size()); ++junctionIndex) {
                if (junctions_[junctionIndex].consistOf(segmentIndicesWithinWindow)) {
                    junctions_[junctionIndex].appendJunctionPoint(supportPoints[pointIndex]);
                    break;
                }
            }
        } else {
            // Cross junction point
			std::vector<int> fourSegmentIndices;
            if (segmentIndicesWithinWindow.size() == 4) {
                fourSegmentIndices = segmentIndicesWithinWindow;
            } else {
                fourSegmentIndices.resize(4);
                for (int i = 0; i < 4; ++i) fourSegmentIndices[i] = segmentIndicesWithinWindow[i];
            }
            bool notAppend = true;
            for (int crossJunctionIndex = 0; crossJunctionIndex < static_cast<int>(crossJunctions_.size()); ++crossJunctionIndex) {
                if (crossJunctions_[crossJunctionIndex].consistOf(fourSegmentIndices)) {
                    crossJunctions_[crossJunctionIndex].appendJunctionPoint(supportPoints[pointIndex]);
                    notAppend = false;
                    break;
                }
            }
            if (notAppend) {
                // Junction point
				std::vector<int> threeSegmentIndices(3);
                for (int i = 0; i < 3; ++i) threeSegmentIndices[i] = segmentIndicesWithinWindow[i];
                for (int junctionIndex = 0; junctionIndex < static_cast<int>(junctions_.size()); ++junctionIndex) {
                    if (junctions_[junctionIndex].consistOf(threeSegmentIndices)) {
                        junctions_[junctionIndex].appendJunctionPoint(supportPoints[pointIndex]);
                        notAppend = false;
                        break;
                    }
                }
            }
            if (notAppend) {
                // Boundary
                for (int boundaryIndex = 0; boundaryIndex < static_cast<int>(boundaries_.size()); ++boundaryIndex) {
                    if (boundaries_[boundaryIndex].consistOf(segmentIndicesWithinWindow[0], segmentIndicesWithinWindow[1])) {
                        boundaries_[boundaryIndex].appendBoundaryPoint(supportPoints[pointIndex]);
                        notAppend = false;
                        break;
                    }
                }
            }
            if (notAppend) {
                throw StereoException("SegmentConfiguration::appendSupportPoint", "can't assign support point");
            }
        }
    }
}

void SegmentConfiguration::set(const rev::Image<unsigned short>& segmentIndexImage,
							   const rev::Image<int>& boundaryIndexImage,
							   const std::vector<Segment>& segments,
							   const std::vector<SegmentBoundary>& boundaries,
							   const std::vector<SegmentJunction>& junctions,
							   const std::vector<SegmentCrossJunction>& crossJunctions)
{
	segmentIndexImage_ = segmentIndexImage;
	boundaryIndexImage_ = boundaryIndexImage;
	segments_ = segments;
	boundaries_ = boundaries;
	junctions_ = junctions;
	crossJunctions_ = crossJunctions;
}

int SegmentConfiguration::countSegmentTotal() const {
    // Image size
    int width = segmentIndexImage_.width();
    int height = segmentIndexImage_.height();
    
    int segmentTotal = 0;
    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            int pixelSegmentIndex = segmentIndexImage_(x, y);
            if (pixelSegmentIndex >= segmentTotal) {
                segmentTotal = pixelSegmentIndex + 1;
            }
        }
    }
    
    return segmentTotal;
}

bool SegmentConfiguration::isHorizontalBoundary(const int x, const int y) const {
    if (x >= segmentIndexImage_.width()-1) return false;
    
    int firstSegmentIndex = segmentIndexImage_(x, y);
    int secondSegmentIndex = segmentIndexImage_(x+1, y);
    
    return (firstSegmentIndex != secondSegmentIndex);
}

bool SegmentConfiguration::isVerticalBoundary(const int x, const int y) const {
    if (y >= segmentIndexImage_.height()-1) return false;
    
    int firstSegmentIndex = segmentIndexImage_(x, y);
    int secondSegmentIndex = segmentIndexImage_(x, y+1);
    
    return (firstSegmentIndex != secondSegmentIndex);
}

int SegmentConfiguration::appendBoundary(const int firstSegmentIndex, const int secondSegmentIndex) {
    int boundaryTotal = static_cast<int>(boundaries_.size());
    for (int boundaryIndex = 0; boundaryIndex < boundaryTotal; ++boundaryIndex) {
        if (boundaries_[boundaryIndex].consistOf(firstSegmentIndex, secondSegmentIndex)) return boundaryIndex;
    }
    
    SegmentBoundary newBoundary(firstSegmentIndex, secondSegmentIndex);
    boundaries_.push_back(newBoundary);
    return static_cast<int>(boundaries_.size())-1;
}

void SegmentConfiguration::setBoundaryIndexImageHorizontal(const int x, const int y, const int boundaryIndex, const int boundaryWidth) {
	int width = boundaryIndexImage_.width();
	int startX = x - boundaryWidth + 1;
	if (startX < 0) startX = 0;
	int endX = x + boundaryWidth;
	if (endX >= width) endX = width - 1;
	for (int positionX = startX; positionX <= endX; ++positionX) {
		boundaryIndexImage_(positionX, y) = boundaryIndex;
	}
}

void SegmentConfiguration::setBoundaryIndexImageVertical(const int x, const int y, const int boundaryIndex, const int boundaryWidth) {
	int height = boundaryIndexImage_.height();
	int startY = y - boundaryWidth + 1;
	if (startY < 0) startY = 0;
	int endY = y + boundaryWidth;
	if (endY >= height) endY = height - 1;
	for (int positionY = startY; positionY <= endY; ++positionY) {
		boundaryIndexImage_(x, positionY) = boundaryIndex;
	}
}

bool SegmentConfiguration::isJunction(const int x, const int y, std::vector<int>& junctionSegmentIndices) const {
    if (x >= segmentIndexImage_.width()-1 || y >= segmentIndexImage_.height()-1) return false;
	
    junctionSegmentIndices.clear();
    for (int offsetY = 0; offsetY <= 1; ++offsetY) {
        for (int offsetX = 0; offsetX <= 1; ++offsetX) {
            int offsetPixelSegmentIndex = segmentIndexImage_(x+offsetX, y+offsetY);
            bool isNewSegmentIndex = true;
            for (int i = 0; i < static_cast<int>(junctionSegmentIndices.size()); ++i) {
                if (offsetPixelSegmentIndex == junctionSegmentIndices[i]) {
                    isNewSegmentIndex = false;
                    break;
                }
            }
            if (isNewSegmentIndex) {
                junctionSegmentIndices.push_back(offsetPixelSegmentIndex);
            }
        }
    }
    
    if (junctionSegmentIndices.size() < 3) return false;
    else return true;
}

void SegmentConfiguration::appendJunction(const int x, const int y, const std::vector<int>& junctionSegmentIndices) {
    if (junctionSegmentIndices.size() == 3) {
        int junctionTotal = static_cast<int>(junctions_.size());
        for (int i = 0; i < junctionTotal; ++i) {
            if (junctions_[i].consistOf(junctionSegmentIndices)) return;
        }
        
        SegmentJunction newJunction(x, y);
        newJunction.setSegmentIndices(junctionSegmentIndices);
        junctions_.push_back(newJunction);
    } else {
        int crossJunctionTotal = static_cast<int>(crossJunctions_.size());
        for (int i = 0; i < crossJunctionTotal; ++i) {
            if (crossJunctions_[i].consistOf(junctionSegmentIndices)) return;
        }
        
        SegmentCrossJunction newCrossJunction(x, y);
        newCrossJunction.setSegmentIndices(junctionSegmentIndices);
        crossJunctions_.push_back(newCrossJunction);
    }
}

void SegmentConfiguration::setBoundaryIndicesInSegment(const int segmentIndex) {
    int boundaryTotal = static_cast<int>(boundaries_.size());
    for (int boundaryIndex = 0; boundaryIndex < boundaryTotal; ++boundaryIndex) {
        if (boundaries_[boundaryIndex].include(segmentIndex) >= 0) {
            segments_[segmentIndex].appendBoundaryIndex(boundaryIndex);
        }
    }
}

void SegmentConfiguration::setBoundaryIndicesInAllJunctions() {
    int boundaryTotal = static_cast<int>(boundaries_.size());
    //    int junctionTotal = static_cast<int>(junctions_.size());
    //    for (int junctionIndex = 0; junctionIndex < junctionTotal; ++junctionIndex) {
    //        int firstSegmentIndex = junctions_[junctionIndex].segmentIndex(0);
    //        int secondSegmentIndex = junctions_[junctionIndex].segmentIndex(1);
    //        int thirdSegmentIndex = junctions_[junctionIndex].segmentIndex(2);
    //		
    //		std::vector<int> junctionBoundaryIndices(3);
    //        junctionBoundaryIndices[0] = -1; junctionBoundaryIndices[1] = -1; junctionBoundaryIndices[2] = -1;
    //        for (int boundaryIndex = 0; boundaryIndex < boundaryTotal; ++boundaryIndex) {
    //            if (boundaries_[boundaryIndex].consistOf(firstSegmentIndex, secondSegmentIndex)) {
    //                junctionBoundaryIndices[0] = boundaryIndex;
    //            } else if (boundaries_[boundaryIndex].consistOf(secondSegmentIndex, thirdSegmentIndex)) {
    //                junctionBoundaryIndices[1] = boundaryIndex;
    //            } else if (boundaries_[boundaryIndex].consistOf(firstSegmentIndex, thirdSegmentIndex)) {
    //                junctionBoundaryIndices[2] = boundaryIndex;
    //            }
    //        }
    //		
    //        junctions_[junctionIndex].setBoundaryIndices(junctionBoundaryIndices);
    //    }
	
	for (std::vector<SegmentJunction>::iterator junctionIterator = junctions_.begin(); junctionIterator != junctions_.end(); ++junctionIterator) {
		int firstSegmentIndex = junctionIterator->segmentIndex(0);
		int secondSegmentIndex = junctionIterator->segmentIndex(1);
		int thirdSegmentIndex = junctionIterator->segmentIndex(2);
		
		std::vector<int> junctionBoundaryIndices(3);
        junctionBoundaryIndices[0] = -1; junctionBoundaryIndices[1] = -1; junctionBoundaryIndices[2] = -1;
        for (int boundaryIndex = 0; boundaryIndex < boundaryTotal; ++boundaryIndex) {
            if (boundaries_[boundaryIndex].consistOf(firstSegmentIndex, secondSegmentIndex)) {
                junctionBoundaryIndices[0] = boundaryIndex;
            } else if (boundaries_[boundaryIndex].consistOf(secondSegmentIndex, thirdSegmentIndex)) {
                junctionBoundaryIndices[1] = boundaryIndex;
            } else if (boundaries_[boundaryIndex].consistOf(firstSegmentIndex, thirdSegmentIndex)) {
                junctionBoundaryIndices[2] = boundaryIndex;
            }
        }
		
		if (junctionBoundaryIndices[0] < 0 || junctionBoundaryIndices[1] < 0 || junctionBoundaryIndices[2] < 0) {
			std::vector<SegmentJunction>::iterator previousIterator = junctionIterator-1;
			junctions_.erase(junctionIterator);
			junctionIterator = previousIterator;
			continue;
		}
		
		junctionIterator->setBoundaryIndices(junctionBoundaryIndices);
	}
}

void SegmentConfiguration::setBoundaryIndicesInAllCrossJunctions() {
    int boundaryTotal = static_cast<int>(boundaries_.size());
    int crossJunctionTotal = static_cast<int>(crossJunctions_.size());
    for (int crossJunctionIndex = 0; crossJunctionIndex < crossJunctionTotal; ++crossJunctionIndex) {
        int firstSegmentIndex = crossJunctions_[crossJunctionIndex].segmentIndex(0);
        int secondSegmentIndex = crossJunctions_[crossJunctionIndex].segmentIndex(1);
        int thirdSegmentIndex = crossJunctions_[crossJunctionIndex].segmentIndex(2);
        int fourthSegmentIndex = crossJunctions_[crossJunctionIndex].segmentIndex(3);
		
		std::vector<int> crossJunctionBoundaryIndices(4);
        for (int i = 0; i < 4; ++i) crossJunctionBoundaryIndices[i] = -1;
		std::vector<bool> crossJunctionBoundaryDirections(4);
        for (int boundaryIndex = 0; boundaryIndex < boundaryTotal; ++boundaryIndex) {
            if (boundaries_[boundaryIndex].consistOf(firstSegmentIndex, secondSegmentIndex)) {
                crossJunctionBoundaryIndices[0] = boundaryIndex;
                if (boundaries_[boundaryIndex].segmentIndex(0) == firstSegmentIndex) crossJunctionBoundaryDirections[0] = true;
                else crossJunctionBoundaryDirections[0] = false;
            } else if (boundaries_[boundaryIndex].consistOf(secondSegmentIndex, fourthSegmentIndex)) {
                crossJunctionBoundaryIndices[1] = boundaryIndex;
                if (boundaries_[boundaryIndex].segmentIndex(0) == secondSegmentIndex) crossJunctionBoundaryDirections[1] = true;
                else crossJunctionBoundaryDirections[1] = false;
            } else if (boundaries_[boundaryIndex].consistOf(thirdSegmentIndex, fourthSegmentIndex)) {
                crossJunctionBoundaryIndices[2] = boundaryIndex;
                if (boundaries_[boundaryIndex].segmentIndex(0) == thirdSegmentIndex) crossJunctionBoundaryDirections[2] = true;
                else crossJunctionBoundaryDirections[2] = false;
            } else if (boundaries_[boundaryIndex].consistOf(firstSegmentIndex, thirdSegmentIndex)) {
                crossJunctionBoundaryIndices[3] = boundaryIndex;
                if (boundaries_[boundaryIndex].segmentIndex(0) == firstSegmentIndex) crossJunctionBoundaryDirections[3] = true;
                else crossJunctionBoundaryDirections[3] = false;
            }
        }
		
        crossJunctions_[crossJunctionIndex].setBoundaryIndices(crossJunctionBoundaryIndices, crossJunctionBoundaryDirections);
    }
}

struct NeighborSegment {
    int segmentIndex;
    int distance;
    
    bool operator<(const NeighborSegment& comparisonNeighbor) const { return (distance < comparisonNeighbor.distance); }
};

std::vector<int> SegmentConfiguration::getSegmentIndicesWithinWindow(const int x, const int y, const int windowRadius) const {
    int width = segmentIndexImage_.width();
    int height = segmentIndexImage_.height();
	
	std::vector<NeighborSegment> neighborSegments;
    for (int offsetY = -windowRadius; offsetY <= windowRadius; ++offsetY) {
        if (y+offsetY < 0 || y+offsetY >= height) continue;
        for (int offsetX = -windowRadius; offsetX <= windowRadius; ++offsetX) {
            if (x+offsetX < 0 || x+offsetX >= width) continue;
            
            int offsetSegmentIndex = segmentIndexImage_(x+offsetX, y+offsetY);
            bool newSegment = true;
            for (int neighborIndex = 0; neighborIndex < static_cast<int>(neighborSegments.size()); ++neighborIndex) {
                if (offsetSegmentIndex == neighborSegments[neighborIndex].segmentIndex) {
                    newSegment = false;
                    if (neighborSegments[neighborIndex].distance > offsetX*offsetX + offsetY*offsetY) {
                        neighborSegments[neighborIndex].distance = offsetX*offsetX + offsetY*offsetY;
                    }
                    break;
                }
            }
            if (newSegment) {
                NeighborSegment newNeighborSegment;
                newNeighborSegment.segmentIndex = offsetSegmentIndex;
                newNeighborSegment.distance = offsetX*offsetX + offsetY*offsetY;
                neighborSegments.push_back(newNeighborSegment);
            }
        }
    }
	std::sort(neighborSegments.begin(), neighborSegments.end());
    
    int neighborTotal = static_cast<int>(neighborSegments.size());
	std::vector<int> segmentIndicesWithinWindow(neighborTotal);
    for (int i = 0; i < neighborTotal; ++i) segmentIndicesWithinWindow[i] = neighborSegments[i].segmentIndex;
    
    return segmentIndicesWithinWindow;
}

void SegmentConfiguration::setSegmentPointDisparity(const int segmentIndex, const int pointIndex, const double disparity) {
    if (segmentIndex < 0 || segmentIndex >= static_cast<int>(segments_.size())) {
        throw StereoException("SegmentConfiguration::setSegmentPointDisparity", "segment index is not valid");
    }
    
    segments_[segmentIndex].setInteriorPointDisparity(pointIndex, disparity);
}

void SegmentConfiguration::setBoundaryPointDisparity(const int boundaryIndex, const int pointIndex, const double disparity) {
    if (boundaryIndex < 0 || boundaryIndex >= static_cast<int>(boundaries_.size())) {
        throw StereoException("SegmentConfiguration::setBoundaryPointDisparity", "boundary index is not valid");
    }
    
    boundaries_[boundaryIndex].setBoundaryPointDisparity(pointIndex, disparity);
}

void SegmentConfiguration::setJunctionPointDisparity(const int junctionIndex, const int pointIndex, const double disparity) {
    if (junctionIndex < 0 || junctionIndex >= static_cast<int>(junctions_.size())) {
        throw StereoException("SegmentConfiguration::setJunctionPointDisparity", "junction index is not valid");
    }
    
    junctions_[junctionIndex].setJunctionPointDisparity(pointIndex, disparity);
}

void SegmentConfiguration::setCrossJunctionPointDisparity(const int crossJunctionIndex, const int pointIndex, const double disparity) {
    if (crossJunctionIndex < 0 || crossJunctionIndex >= static_cast<int>(crossJunctions_.size())) {
        throw StereoException("SegmentConfiguration::setCrossJunctionPointDisparity", "cross junction index is not valid");
    }
    
    crossJunctions_[crossJunctionIndex].setJunctionPointDisparity(pointIndex, disparity);
}

void SegmentConfiguration::writeToFile(const std::string& outputFilename) {
	std::ofstream outputFileStream;
    outputFileStream.open(outputFilename.c_str(), std::ios_base::out | std::ios_base::binary);
    if (outputFileStream.fail()) {
		std::stringstream errorMessage;
        errorMessage << "can't open output file (" << outputFilename << ")";
        throw StereoException("SegmentConfiguration::writeToFile", errorMessage.str());
    }
    
    // Image size
    int width = segmentIndexImage_.width();
    int height = segmentIndexImage_.height();
    outputFileStream.write(reinterpret_cast<char*>(&width), sizeof(int));
    outputFileStream.write(reinterpret_cast<char*>(&height), sizeof(int));
    // Max number of disparity candidates of support points
    int candidateMax = supportPointDisparityCandidateMax_;
    outputFileStream.write(reinterpret_cast<char*>(&candidateMax), sizeof(int));
    
    // Superpixel
    // Segment index image data
    int* segmentImageData = new int [width*height];
    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            segmentImageData[width*y+x] = segmentIndexImage_(x, y);
        }
    }
    outputFileStream.write(reinterpret_cast<char*>(segmentImageData), width*height*sizeof(int));
    delete [] segmentImageData;
	
	// Boundary index image
	int* boundaryImageData = new int [width*height];
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			boundaryImageData[width*y+x] = boundaryIndexImage_(x, y);
		}
	}
	outputFileStream.write(reinterpret_cast<char*>(boundaryImageData), width*height*sizeof(int));
	delete [] boundaryImageData;
	
    // Segment
    // The number of segments
    int segmentTotal = static_cast<int>(segments_.size());
    outputFileStream.write(reinterpret_cast<char*>(&segmentTotal), sizeof(int));
    // Segment center points
    for (int segmentIndex = 0; segmentIndex < segmentTotal; ++segmentIndex) {
        double centerPoint[2];
        centerPoint[0] = segments_[segmentIndex].centerX();
        centerPoint[1] = segments_[segmentIndex].centerY();
        outputFileStream.write(reinterpret_cast<char*>(centerPoint), 2*sizeof(double));
    }
    // Segment pixels
    for (int segmentIndex = 0; segmentIndex < segmentTotal; ++segmentIndex) {
        int pixelTotal = segments_[segmentIndex].segmentPixelTotal();
        outputFileStream.write(reinterpret_cast<char*>(&pixelTotal), sizeof(int));
        for (int pixelIndex = 0; pixelIndex < pixelTotal; ++pixelIndex) {
            int pixelPosition[2];
            pixelPosition[0] = segments_[segmentIndex].segmentPixelX(pixelIndex);
            pixelPosition[1] = segments_[segmentIndex].segmentPixelY(pixelIndex);
            outputFileStream.write(reinterpret_cast<char*>(pixelPosition), 2*sizeof(int));
        }
    }
    // Interior points
    for (int segmentIndex = 0; segmentIndex < segmentTotal; ++segmentIndex) {
        int pointTotal = segments_[segmentIndex].interiorPointTotal();
        outputFileStream.write(reinterpret_cast<char*>(&pointTotal), sizeof(int));
        // Positions
        for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
            int position[2];
            position[0] = segments_[segmentIndex].interiorPoint(pointIndex).x();
            position[1] = segments_[segmentIndex].interiorPoint(pointIndex).y();
            outputFileStream.write(reinterpret_cast<char*>(position), 2*sizeof(int));
        }
        // Disparities
        for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
            int disparityTotal = segments_[segmentIndex].interiorPoint(pointIndex).disparityTotal();
            outputFileStream.write(reinterpret_cast<char*>(&disparityTotal), sizeof(int));
            for (int candidateIndex = 0; candidateIndex < disparityTotal; ++candidateIndex) {
                double disparity = segments_[segmentIndex].interiorPoint(pointIndex).disparity(candidateIndex);
                double error = segments_[segmentIndex].interiorPoint(pointIndex).disparityError(candidateIndex);
                outputFileStream.write(reinterpret_cast<char*>(&disparity), sizeof(double));
                outputFileStream.write(reinterpret_cast<char*>(&error), sizeof(double));
            }
        }
    }
    // Boundary indices
    for (int segmentIndex = 0; segmentIndex < segmentTotal; ++segmentIndex) {
        int boundaryTotal = segments_[segmentIndex].boundaryTotal();
        outputFileStream.write(reinterpret_cast<char*>(&boundaryTotal), sizeof(int));
        for (int segmentBoundaryIndex = 0; segmentBoundaryIndex < boundaryTotal; ++segmentBoundaryIndex) {
            int boundaryIndex = segments_[segmentIndex].boundaryIndex(segmentBoundaryIndex);
            outputFileStream.write(reinterpret_cast<char*>(&boundaryIndex), sizeof(int));
        }
    }
    
    // Boundary
    // The number of boundaries
    int boundaryTotal = static_cast<int>(boundaries_.size());
    outputFileStream.write(reinterpret_cast<char*>(&boundaryTotal), sizeof(int));
    // Segment indices of boundaries
    for (int boundaryIndex = 0; boundaryIndex < boundaryTotal; ++boundaryIndex) {
        int segmentIndices[2];
        segmentIndices[0] = boundaries_[boundaryIndex].segmentIndex(0);
        segmentIndices[1] = boundaries_[boundaryIndex].segmentIndex(1);
        outputFileStream.write(reinterpret_cast<char*>(segmentIndices), 2*sizeof(int));
    }
    // Boundary pixels
    for (int boundaryIndex = 0; boundaryIndex < boundaryTotal; ++boundaryIndex) {
        int pixelTotal = boundaries_[boundaryIndex].boundaryPixelTotal();
        outputFileStream.write(reinterpret_cast<char*>(&pixelTotal), sizeof(int));
        for (int pixelIndex = 0; pixelIndex < pixelTotal; ++pixelIndex) {
            double pixelPosition[2];
            pixelPosition[0] = boundaries_[boundaryIndex].boundaryPixelX(pixelIndex);
            pixelPosition[1] = boundaries_[boundaryIndex].boundaryPixelY(pixelIndex);
            outputFileStream.write(reinterpret_cast<char*>(pixelPosition), 2*sizeof(double));
        }
    }
    // Boundary points
    for (int boundaryIndex = 0; boundaryIndex < boundaryTotal; ++boundaryIndex) {
        int pointTotal = boundaries_[boundaryIndex].boundaryPointTotal();
        outputFileStream.write(reinterpret_cast<char*>(&pointTotal), sizeof(int));
        // Positions
        for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
            int position[2];
            position[0] = boundaries_[boundaryIndex].boundaryPoint(pointIndex).x();
            position[1] = boundaries_[boundaryIndex].boundaryPoint(pointIndex).y();
            outputFileStream.write(reinterpret_cast<char*>(position), 2*sizeof(int));
        }
        // Disparities
        for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
            int disparityTotal = boundaries_[boundaryIndex].boundaryPoint(pointIndex).disparityTotal();
            outputFileStream.write(reinterpret_cast<char*>(&disparityTotal), sizeof(int));
            for (int candidateIndex = 0; candidateIndex < disparityTotal; ++candidateIndex) {
                double disparity = boundaries_[boundaryIndex].boundaryPoint(pointIndex).disparity(candidateIndex);
                double error = boundaries_[boundaryIndex].boundaryPoint(pointIndex).disparityError(candidateIndex);
                outputFileStream.write(reinterpret_cast<char*>(&disparity), sizeof(double));
                outputFileStream.write(reinterpret_cast<char*>(&error), sizeof(double));
            }
        }
    }
    
    // Junction
    // The number of junctions
    int junctionTotal = static_cast<int>(junctions_.size());
    outputFileStream.write(reinterpret_cast<char*>(&junctionTotal), sizeof(int));
    // Junction position
    for (int junctionIndex = 0; junctionIndex < junctionTotal; ++junctionIndex) {
        int position[2];
        position[0] = junctions_[junctionIndex].x();
        position[1] = junctions_[junctionIndex].y();
        outputFileStream.write(reinterpret_cast<char*>(position), 2*sizeof(int));
    }
    // Segment indices of junctions
    for (int junctionIndex = 0; junctionIndex < junctionTotal; ++junctionIndex) {
        int segmentIndices[3];
        segmentIndices[0] = junctions_[junctionIndex].segmentIndex(0);
        segmentIndices[1] = junctions_[junctionIndex].segmentIndex(1);
        segmentIndices[2] = junctions_[junctionIndex].segmentIndex(2);
        outputFileStream.write(reinterpret_cast<char*>(segmentIndices), 3*sizeof(int));
    }
    // Boundary indices of junctions
    for (int junctionIndex = 0; junctionIndex < junctionTotal; ++junctionIndex) {
        int boundaryIndices[3];
        boundaryIndices[0] = junctions_[junctionIndex].boundaryIndex(0);
        boundaryIndices[1] = junctions_[junctionIndex].boundaryIndex(1);
        boundaryIndices[2] = junctions_[junctionIndex].boundaryIndex(2);
        outputFileStream.write(reinterpret_cast<char*>(boundaryIndices), 3*sizeof(int));
    }
    // Junction points
    for (int junctionIndex = 0; junctionIndex < junctionTotal; ++junctionIndex) {
        int pointTotal = junctions_[junctionIndex].junctionPointTotal();
        outputFileStream.write(reinterpret_cast<char*>(&pointTotal), sizeof(int));
        // Positions
        for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
            int position[2];
            position[0] = junctions_[junctionIndex].junctionPoint(pointIndex).x();
            position[1] = junctions_[junctionIndex].junctionPoint(pointIndex).y();
            outputFileStream.write(reinterpret_cast<char*>(position), 2*sizeof(int));
        }
        // Disparities
        for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
            int disparityTotal = junctions_[junctionIndex].junctionPoint(pointIndex).disparityTotal();
            outputFileStream.write(reinterpret_cast<char*>(&disparityTotal), sizeof(int));
            for (int candidateIndex = 0; candidateIndex < disparityTotal; ++candidateIndex) {
                double disparity = junctions_[junctionIndex].junctionPoint(pointIndex).disparity(candidateIndex);
                double error = junctions_[junctionIndex].junctionPoint(pointIndex).disparityError(candidateIndex);
                outputFileStream.write(reinterpret_cast<char*>(&disparity), sizeof(double));
                outputFileStream.write(reinterpret_cast<char*>(&error), sizeof(double));
            }
        }
    }
	
    // Cross junction
    // The number of cross junctions
    int crossJunctionTotal = static_cast<int>(crossJunctions_.size());
    outputFileStream.write(reinterpret_cast<char*>(&crossJunctionTotal), sizeof(int));
    // Cross junction position
    for (int crossJunctionIndex = 0; crossJunctionIndex < crossJunctionTotal; ++crossJunctionIndex) {
        int position[2];
        position[0] = crossJunctions_[crossJunctionIndex].x();
        position[1] = crossJunctions_[crossJunctionIndex].y();
        outputFileStream.write(reinterpret_cast<char*>(position), 2*sizeof(int));
    }
    // Segment indices of cross junctions
    for (int crossJunctionIndex = 0; crossJunctionIndex < crossJunctionTotal; ++crossJunctionIndex) {
        int segmentIndices[4];
        segmentIndices[0] = crossJunctions_[crossJunctionIndex].segmentIndex(0);
        segmentIndices[1] = crossJunctions_[crossJunctionIndex].segmentIndex(1);
        segmentIndices[2] = crossJunctions_[crossJunctionIndex].segmentIndex(2);
        segmentIndices[3] = crossJunctions_[crossJunctionIndex].segmentIndex(3);
        outputFileStream.write(reinterpret_cast<char*>(segmentIndices), 4*sizeof(int));
    }
    // Boundary indices and directions of cross junctions
    for (int crossJunctionIndex = 0; crossJunctionIndex < crossJunctionTotal; ++crossJunctionIndex) {
        int boundaryIndices[4];
        boundaryIndices[0] = crossJunctions_[crossJunctionIndex].boundaryIndex(0);
        boundaryIndices[1] = crossJunctions_[crossJunctionIndex].boundaryIndex(1);
        boundaryIndices[2] = crossJunctions_[crossJunctionIndex].boundaryIndex(2);
        boundaryIndices[3] = crossJunctions_[crossJunctionIndex].boundaryIndex(3);
        outputFileStream.write(reinterpret_cast<char*>(boundaryIndices), 4*sizeof(int));
        int boundaryDirections[4];
        boundaryDirections[0] = static_cast<int>(crossJunctions_[crossJunctionIndex].boundaryDirection(0));
        boundaryDirections[1] = static_cast<int>(crossJunctions_[crossJunctionIndex].boundaryDirection(1));
        boundaryDirections[2] = static_cast<int>(crossJunctions_[crossJunctionIndex].boundaryDirection(2));
        boundaryDirections[3] = static_cast<int>(crossJunctions_[crossJunctionIndex].boundaryDirection(3));
        outputFileStream.write(reinterpret_cast<char*>(boundaryDirections), 4*sizeof(int));
    }
    // Cross junction points
    for (int crossJunctionIndex = 0; crossJunctionIndex < crossJunctionTotal; ++crossJunctionIndex) {
        int pointTotal = crossJunctions_[crossJunctionIndex].junctionPointTotal();
        outputFileStream.write(reinterpret_cast<char*>(&pointTotal), sizeof(int));
        // Positions
        for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
            int position[2];
            position[0] = crossJunctions_[crossJunctionIndex].junctionPoint(pointIndex).x();
            position[1] = crossJunctions_[crossJunctionIndex].junctionPoint(pointIndex).y();
            outputFileStream.write(reinterpret_cast<char*>(position), 2*sizeof(int));
        }
        // Disparities
        for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
            int disparityTotal = crossJunctions_[crossJunctionIndex].junctionPoint(pointIndex).disparityTotal();
            outputFileStream.write(reinterpret_cast<char*>(&disparityTotal), sizeof(int));
            for (int candidateIndex = 0; candidateIndex < disparityTotal; ++candidateIndex) {
                double disparity = crossJunctions_[crossJunctionIndex].junctionPoint(pointIndex).disparity(candidateIndex);
                double error = crossJunctions_[crossJunctionIndex].junctionPoint(pointIndex).disparityError(candidateIndex);
                outputFileStream.write(reinterpret_cast<char*>(&disparity), sizeof(double));
                outputFileStream.write(reinterpret_cast<char*>(&error), sizeof(double));
            }
        }
    }
    outputFileStream.close();
}

void SegmentConfiguration::setFromFile(const std::string& configurationFilename) {
    // Open file
	std::ifstream inputFileStream;
    inputFileStream.open(configurationFilename.c_str(), std::ios_base::in | std::ios_base::binary);
    if (!inputFileStream) {
		std::stringstream errorMessage;
        errorMessage << "can't open input file (" << configurationFilename << ")";
        throw StereoException("SegmentConfiguration::setFromFile", errorMessage.str());
    }

    // Image size
    int width;
    int height;
    inputFileStream.read(reinterpret_cast<char*>(&width), sizeof(int));
    inputFileStream.read(reinterpret_cast<char*>(&height), sizeof(int));
    // Max number of disparity candidates of support points
    int candidateMax;
    inputFileStream.read(reinterpret_cast<char*>(&candidateMax), sizeof(int));
    
    // Superpixel
    int* segmentImageData = new int [width*height];
    inputFileStream.read(reinterpret_cast<char*>(segmentImageData), width*height*sizeof(int));
	segmentIndexImage_.resize(width, height, 1);
    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            segmentIndexImage_(x, y) = segmentImageData[width*y+x];
        }
    }
    delete [] segmentImageData;
	
	// Boundary index image
	int* boundaryImageData = new int [width*height];
	inputFileStream.read(reinterpret_cast<char*>(boundaryImageData), width*height*sizeof(int));
	boundaryIndexImage_.resize(width, height, 1);
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			boundaryIndexImage_(x, y) = boundaryImageData[width*y+x];
		}
	}
	delete [] boundaryImageData;
    
    // Segment
    // The number of segments
    int segmentTotal;
    inputFileStream.read(reinterpret_cast<char*>(&segmentTotal), sizeof(int));
    segments_.resize(segmentTotal);
    // Segment center points
    for (int segmentIndex = 0; segmentIndex < segmentTotal; ++segmentIndex) {
        double centerPoint[2];
        inputFileStream.read(reinterpret_cast<char*>(centerPoint), 2*sizeof(double));
        segments_[segmentIndex].setCenter(centerPoint[0], centerPoint[1]);
    }
    // Segment pixels
    for (int segmentIndex = 0; segmentIndex < segmentTotal; ++segmentIndex) {
        int pixelTotal;
        inputFileStream.read(reinterpret_cast<char*>(&pixelTotal), sizeof(int));
        for (int pixelIndex = 0; pixelIndex < pixelTotal; ++pixelIndex) {
            int pixelPosition[2];
            inputFileStream.read(reinterpret_cast<char*>(pixelPosition), 2*sizeof(int));
            segments_[segmentIndex].appendSegmentPixel(pixelPosition[0], pixelPosition[1]);
        }
    }
    // Interior points
    for (int segmentIndex = 0; segmentIndex < segmentTotal; ++segmentIndex) {
        int pointTotal;
        inputFileStream.read(reinterpret_cast<char*>(&pointTotal), sizeof(int));
		std::vector<SupportPoint> interiorPoints(pointTotal);
        // Position
        for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
            int position[2];
            inputFileStream.read(reinterpret_cast<char*>(position), 2*sizeof(int));
            interiorPoints[pointIndex].set(position[0], position[1]);
        }
        // Disparity
        for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
            int disparityTotal;
            inputFileStream.read(reinterpret_cast<char*>(&disparityTotal), sizeof(int));
            for (int candidateIndex = 0; candidateIndex < disparityTotal; ++candidateIndex) {
                double disparity, error;
                inputFileStream.read(reinterpret_cast<char*>(&disparity), sizeof(double));
                inputFileStream.read(reinterpret_cast<char*>(&error), sizeof(double));
                interiorPoints[pointIndex].appendDisparity(disparity, error);
            }
        }
        // Set interior points
        for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
            segments_[segmentIndex].appendInteriorPoint(interiorPoints[pointIndex]);
        }
    }
    // Boundary indices
    for (int segmentIndex = 0; segmentIndex < segmentTotal; ++segmentIndex) {
        int boundaryTotal;
        inputFileStream.read(reinterpret_cast<char*>(&boundaryTotal), sizeof(int));
        for (int segmentBoundaryIndex = 0; segmentBoundaryIndex < boundaryTotal; ++segmentBoundaryIndex) {
            int boundaryIndex;
            inputFileStream.read(reinterpret_cast<char*>(&boundaryIndex), sizeof(int));
            segments_[segmentIndex].appendBoundaryIndex(boundaryIndex);
        }
    }
    
    // Boundary
    // The number of boundaries
    int boundaryTotal;
    inputFileStream.read(reinterpret_cast<char*>(&boundaryTotal), sizeof(int));
    boundaries_.resize(boundaryTotal);
    // Segment indices of boundaries
    for (int boundaryIndex = 0; boundaryIndex < boundaryTotal; ++boundaryIndex) {
        int segmentIndices[2];
        inputFileStream.read(reinterpret_cast<char*>(segmentIndices), 2*sizeof(int));
        boundaries_[boundaryIndex].set(segmentIndices[0], segmentIndices[1]);
    }
    // Boundary pixels
    for (int boundaryIndex = 0; boundaryIndex < boundaryTotal; ++boundaryIndex) {
        int pixelTotal;
        inputFileStream.read(reinterpret_cast<char*>(&pixelTotal), sizeof(int));
        for (int pixelIndex = 0; pixelIndex < pixelTotal; ++pixelIndex) {
            double pixelPosition[2];
            inputFileStream.read(reinterpret_cast<char*>(pixelPosition), 2*sizeof(double));
            boundaries_[boundaryIndex].appendBoundaryPixel(pixelPosition[0], pixelPosition[1]);
        }
    }
    // Boundary points
    for (int boundaryIndex = 0; boundaryIndex < boundaryTotal; ++boundaryIndex) {
        int pointTotal;
        inputFileStream.read(reinterpret_cast<char*>(&pointTotal), sizeof(int));
		std::vector<SupportPoint> boundaryPoints(pointTotal);
        // Position
        for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
            int position[2];
            inputFileStream.read(reinterpret_cast<char*>(position), 2*sizeof(int));
            boundaryPoints[pointIndex].set(position[0], position[1]);
        }
        // Disparity
        for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
            int disparityTotal;
            inputFileStream.read(reinterpret_cast<char*>(&disparityTotal), sizeof(int));
            for (int candidateIndex = 0; candidateIndex < disparityTotal; ++candidateIndex) {
                double disparity, error;
                inputFileStream.read(reinterpret_cast<char*>(&disparity), sizeof(double));
                inputFileStream.read(reinterpret_cast<char*>(&error), sizeof(double));
                boundaryPoints[pointIndex].appendDisparity(disparity, error);
            }
        }
        // Set boundary points
        for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
            boundaries_[boundaryIndex].appendBoundaryPoint(boundaryPoints[pointIndex]);
        }
    }
    
    // Junction
    // The number of junctions
    int junctionTotal;
    inputFileStream.read(reinterpret_cast<char*>(&junctionTotal), sizeof(int));
    junctions_.resize(junctionTotal);
    // Position
    for (int junctionIndex = 0; junctionIndex < junctionTotal; ++junctionIndex) {
        int position[2];
        inputFileStream.read(reinterpret_cast<char*>(position), 2*sizeof(int));
        junctions_[junctionIndex].setPosition(position[0], position[1]);
    }
    // Segment indices
    for (int junctionIndex = 0; junctionIndex < junctionTotal; ++junctionIndex) {
        int indices[3];
        inputFileStream.read(reinterpret_cast<char*>(indices), 3*sizeof(int));
		std::vector<int> segmentIndices(3);
        segmentIndices[0] = indices[0]; segmentIndices[1] = indices[1]; segmentIndices[2] = indices[2];
        junctions_[junctionIndex].setSegmentIndices(segmentIndices);
    }
    // Boundary indices
    for (int junctionIndex = 0; junctionIndex < junctionTotal; ++junctionIndex) {
        int indices[3];
        inputFileStream.read(reinterpret_cast<char*>(indices), 3*sizeof(int));
		std::vector<int> boundaryIndices(3);
        boundaryIndices[0] = indices[0]; boundaryIndices[1] = indices[1]; boundaryIndices[2] = indices[2];
        junctions_[junctionIndex].setBoundaryIndices(boundaryIndices);
    }
    // Junction points
    for (int junctionIndex = 0; junctionIndex < junctionTotal; ++junctionIndex) {
        int pointTotal;
        inputFileStream.read(reinterpret_cast<char*>(&pointTotal), sizeof(int));
		std::vector<SupportPoint> junctionPoints(pointTotal);
        // Position
        for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
            int position[2];
            inputFileStream.read(reinterpret_cast<char*>(position), 2*sizeof(int));
            junctionPoints[pointIndex].set(position[0], position[1]);
        }
        // Disparity
        for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
            int disparityTotal;
            inputFileStream.read(reinterpret_cast<char*>(&disparityTotal), sizeof(int));
            for (int candidateIndex = 0; candidateIndex < disparityTotal; ++candidateIndex) {
                double disparity, error;
                inputFileStream.read(reinterpret_cast<char*>(&disparity), sizeof(double));
                inputFileStream.read(reinterpret_cast<char*>(&error), sizeof(double));
                junctionPoints[pointIndex].appendDisparity(disparity, error);
            }
        }
        // Set junction points
        for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
            junctions_[junctionIndex].appendJunctionPoint(junctionPoints[pointIndex]);
        }
    }
    
    // Cross junction
    // The number of cross junctions
    int crossJunctionTotal;
    inputFileStream.read(reinterpret_cast<char*>(&crossJunctionTotal), sizeof(int));
    crossJunctions_.resize(crossJunctionTotal);
    // Position
    for (int crossJunctionIndex = 0; crossJunctionIndex < crossJunctionTotal; ++crossJunctionIndex) {
        int position[2];
        inputFileStream.read(reinterpret_cast<char*>(position), 2*sizeof(int));
        crossJunctions_[crossJunctionIndex].setPosition(position[0], position[1]);
    }
    // Segment indices
    for (int crossJunctionIndex = 0; crossJunctionIndex < crossJunctionTotal; ++crossJunctionIndex) {
        int indices[4];
        inputFileStream.read(reinterpret_cast<char*>(indices), 4*sizeof(int));
		std::vector<int> segmentIndices(4);
        segmentIndices[0] = indices[0]; segmentIndices[1] = indices[1];
        segmentIndices[2] = indices[2]; segmentIndices[3] = indices[3];
        crossJunctions_[crossJunctionIndex].setSegmentIndices(segmentIndices);
    }
    // Boundary indices
    for (int crossJunctionIndex = 0; crossJunctionIndex < crossJunctionTotal; ++crossJunctionIndex) {
        int indices[4];
        inputFileStream.read(reinterpret_cast<char*>(indices), 4*sizeof(int));
		std::vector<int> boundaryIndices(4);
        boundaryIndices[0] = indices[0]; boundaryIndices[1] = indices[1];
        boundaryIndices[2] = indices[2]; boundaryIndices[3] = indices[3];
        int directions[4];
        inputFileStream.read(reinterpret_cast<char*>(directions), 4*sizeof(int));
		std::vector<bool> boundaryDirections(4);
        boundaryDirections[0] = directions[0]; boundaryDirections[1] = directions[1];
        boundaryDirections[2] = directions[2]; boundaryDirections[3] = directions[3];
        crossJunctions_[crossJunctionIndex].setBoundaryIndices(boundaryIndices, boundaryDirections);
    }
    // Cross junction points
    for (int crossJunctionIndex = 0; crossJunctionIndex < crossJunctionTotal; ++crossJunctionIndex) {
        int pointTotal;
        inputFileStream.read(reinterpret_cast<char*>(&pointTotal), sizeof(int));
		std::vector<SupportPoint> crossJunctionPoints(pointTotal);
        // Position
        for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
            int position[2];
            inputFileStream.read(reinterpret_cast<char*>(position), 2*sizeof(int));
            crossJunctionPoints[pointIndex].set(position[0], position[1]);
        }
        // Disparity
        for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
            int disparityTotal;
            inputFileStream.read(reinterpret_cast<char*>(&disparityTotal), sizeof(int));
            for (int candidateIndex = 0; candidateIndex < disparityTotal; ++candidateIndex) {
                double disparity, error;
                inputFileStream.read(reinterpret_cast<char*>(&disparity), sizeof(double));
                inputFileStream.read(reinterpret_cast<char*>(&error), sizeof(double));
                crossJunctionPoints[pointIndex].appendDisparity(disparity, error);
            }
        }
        // Set junction points
        for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
            crossJunctions_[crossJunctionIndex].appendJunctionPoint(crossJunctionPoints[pointIndex]);
        }
    }
    inputFileStream.close();
}
