#include <iostream>
#include <string>
#include <time.h>
#include <cmdline.h>
#include <revlib.h>
#include "CameraMotion.h"
#include "SgmFlow.h"

struct ParameterSgmFlow {
	bool verbose;
	double maxVZRatio;
	int discretizationTotal;
	bool outputVZIndexImage;
	std::string firstImageFilename;
	std::string secondImageFilename;
	std::string calibrationFilename;
	std::string fundamentalMatrixFilename;
	std::string outputFlowImageFilename;
	std::string outputFirstVZIndexImageFilename;
	std::string outputSecondVZIndexImageFilename;
};

// Prototype declaration
ParameterSgmFlow parseCommandline(int argc, char* argv[]);

ParameterSgmFlow parseCommandline(int argc, char* argv[]) {
	// Make command parser
	cmdline::parser commandParser;
	commandParser.add<std::string>("output", 'o', "output flow image", false, "");
	commandParser.add<double>("max", 'm', "max value of VZ-ratio", false, 0.3);
	commandParser.add<int>("number", 'n', "the number of discretization of VZ-ratio", false, 256);
	commandParser.add("ind", 'i', "output VZ-index images");
	commandParser.add("verbose", 'v', "verbose");
	commandParser.add("help", 'h', "display this message");
	commandParser.set_program_name("sgmflow");
	commandParser.footer("first_image second_image calib_file fundamental_matrix");

	// Parse command line
	bool isCorrectCommand = commandParser.parse(argc, argv);
	if (!isCorrectCommand) {
		std::cerr << commandParser.error() << std::endl;
	}
	if (!isCorrectCommand || commandParser.exist("help") || commandParser.rest().size() < 4) {
		std::cerr << commandParser.usage() << std::endl;
		exit(1);
	}

	// Set program parameters
	ParameterSgmFlow parameters;
	// Verbose flag
	parameters.verbose = commandParser.exist("verbose");
	// Max VZ-ratio
	parameters.maxVZRatio = commandParser.get<double>("max");
	// The number of discretization
	parameters.discretizationTotal = commandParser.get<int>("number");
	// Output flag of VZ-index image
	parameters.outputVZIndexImage = commandParser.exist("ind");
	// Input files
	parameters.firstImageFilename = commandParser.rest()[0];
	parameters.secondImageFilename = commandParser.rest()[1];
	parameters.calibrationFilename = commandParser.rest()[2];
	parameters.fundamentalMatrixFilename = commandParser.rest()[3];
	// Output files
	std::string outputFlowImageFilname = commandParser.get<std::string>("output");
	std::string outputVZIndexBaseFilename;
	if (outputFlowImageFilname != "") {
		outputVZIndexBaseFilename = outputFlowImageFilname;
		size_t dotPosition= outputVZIndexBaseFilename.rfind('.');
		if (dotPosition != std::string::npos) outputVZIndexBaseFilename.erase(dotPosition);
	} else {
		outputFlowImageFilname = parameters.firstImageFilename;
		size_t dotPosition = outputFlowImageFilname.rfind('.');
		if (dotPosition != std::string::npos) outputFlowImageFilname.erase(dotPosition);
		outputVZIndexBaseFilename = outputFlowImageFilname;
		outputFlowImageFilname += "_flow.png";
	}
	parameters.outputFlowImageFilename = outputFlowImageFilname;
	parameters.outputFirstVZIndexImageFilename = outputVZIndexBaseFilename + "_first_vz.png";
	parameters.outputSecondVZIndexImageFilename = outputVZIndexBaseFilename + "_second_vz.png";

	return parameters;
}

int main(int argc, char* argv[]) {
	ParameterSgmFlow parameters = parseCommandline(argc, argv);

	if (parameters.verbose) {
		std::cerr << std::endl;
		std::cerr << "First image:        " << parameters.firstImageFilename << std::endl;
		std::cerr << "Second image:       " << parameters.secondImageFilename << std::endl;
		std::cerr << "Calibration file:   " << parameters.calibrationFilename << std::endl;
		std::cerr << "Fundametal matrix:  " << parameters.fundamentalMatrixFilename << std::endl;
		std::cerr << "Output flow image:  " << parameters.outputFlowImageFilename << std::endl;
		if (parameters.outputVZIndexImage) {
			std::cerr << "Output first VZ-index image:  " << parameters.outputFirstVZIndexImageFilename << std::endl;
			std::cerr << "Output second VZ-index image:  " << parameters.outputSecondVZIndexImageFilename << std::endl;
		}
		std::cerr << "   Max VZ-ratio:    " << parameters.maxVZRatio << std::endl;
		std::cerr << "   #discretization: " << parameters.discretizationTotal << std::endl;
		std::cerr << std::endl;
	}

	try {
		// Open input files
		rev::Image<unsigned char> firstImage = rev::readImageFile(parameters.firstImageFilename);
		rev::Image<unsigned char> secondImage = rev::readImageFile(parameters.secondImageFilename);

		clock_t startClock, endClock;
		startClock = clock();

		CameraMotion cameraMotion;
		cameraMotion.initialize(parameters.calibrationFilename, parameters.fundamentalMatrixFilename);

		// SGM-Flow
		SgmFlow sgm;
		sgm.setVZRatioParameters(parameters.maxVZRatio, parameters.discretizationTotal);
		rev::Image<unsigned short> firstVZIndexImage, secondVZIndexImage;
		rev::Image<unsigned short> firstFlowImage;
		sgm.compute(firstImage, secondImage, cameraMotion,
			firstVZIndexImage, secondVZIndexImage, firstFlowImage);

		endClock = clock();
		if (parameters.verbose) std::cerr << "Time: " << static_cast<double>(endClock - startClock)/CLOCKS_PER_SEC << std::endl;

		// Output
		rev::write16bitImageFile(parameters.outputFlowImageFilename, firstFlowImage);
		if (parameters.outputVZIndexImage) {
			rev::write16bitImageFile(parameters.outputFirstVZIndexImageFilename, firstVZIndexImage);
			rev::write16bitImageFile(parameters.outputSecondVZIndexImageFilename, secondVZIndexImage);
		}

	} catch (const rev::Exception& revException) {
		std::cerr << "Error [" << revException.functionName() << "]: " << revException.message() << std::endl;
		exit(1);
	}
}
