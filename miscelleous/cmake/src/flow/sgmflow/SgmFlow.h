#pragma once

#include <revlib.h>
#include "CameraMotion.h"
#include "CalibratedEpipolarFlowGeometry.h"

class SgmFlow {
public:
	SgmFlow();

	void setVZRatioParameters(const double maxVZRatio, const int discretizationTotal);
	void setDataCostParameters(const int sobelCapValue,
		const int censusWindowRadius,
		const int censusWeightFactor,
		const int aggregationWindowRadius);
	void setSmoothnessCostParameters(const int smoothnessPenaltySmall, const int smoothnessPenaltyLarge);
	void setSgmParameters(const bool pathEightDirections, const int consistencyThreshold);

	void compute(const rev::Image<unsigned char>& firstImage,
		const rev::Image<unsigned char>& secondImage,
		const CameraMotion& cameraMotion,
		rev::Image<unsigned short>& firstVZIndexImage,
		rev::Image<unsigned short>& secondVZIndexImage,
		rev::Image<unsigned short>& firstFlowImage);

private:
	void checkInputImage(const rev::Image<unsigned char>& firstImage,
		const rev::Image<unsigned char>& secondImage) const;
	void allocateBuffer(const int width, const int height);
	void performSGM(const rev::Image<unsigned short>& costImage, rev::Image<unsigned short>& vzIndexImage);
	void convertVZIndexImageFromFirstToSecond(const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry,
		const rev::Image<unsigned short>& firstVZIndexImage,
		rev::Image<unsigned short>& secondVZIndexImage) const;
	void enforceLeftRightConsistency(const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry,
		rev::Image<unsigned short>& firstVZIndexImage,
		rev::Image<unsigned short>& secondVZIndexImage) const;
	void makeFlowImage(const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry,
		const rev::Image<unsigned short>& firstVZIndexImage,
		rev::Image<unsigned short>& firstFlowImage) const;

	void setBufferSize(const int width, const int height);
	void speckleFilter(const int maxSpeckleSize, const int maxDifference,
		rev::Image<unsigned short>& image) const;
	void interpolateVZIndexImage(const rev::Image<unsigned short>& vzIndexImage,
		rev::Image<unsigned short>& interpolatedVZIndexImage) const;

	// Parameter
	double maxVZRatio_;
	int discretizationTotal_;
	int sobelCapValue_;
	int censusWindowRadius_;
	double censusWeightFactor_;
	int aggregationWindowRadius_;
	int smoothnessPenaltySmall_;
	int smoothnessPenaltyLarge_;
	bool pathEightDirections_;
	int consistencyThreshold_;

	// Data
	int pathRowBufferTotal_;
	int vzIndexSize_;
	int pathTotal_;
	int pathVZIndexSize_;
	int costSumBufferRowSize_;
	int costSumBufferSize_;
	int pathMinCostBufferSize_;
	int pathCostBufferSize_;
	int totalBufferSize_;
	rev::AlignedMemory<short> buffer_;
};
