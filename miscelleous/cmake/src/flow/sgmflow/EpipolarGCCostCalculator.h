#pragma once

#include <revlib.h>
#include "CalibratedEpipolarFlowGeometry.h"

class EpipolarGCCostCalculator {
public:
	EpipolarGCCostCalculator();
	EpipolarGCCostCalculator(const double maxVZRatio, const int discretizationTotal,
		const int sobelCapValue,
		const int censusWindowRadius, const double censusWeightFactor,
		const int aggregationWindowRadius);

	void setParameter(const double maxVZRatio, const int discretizationTotal,
		const int sobelCapValue,
		const int censusWindowRadius, const double censusWeightFactor,
		const int aggregationWindowRadius);

	void computeCostImage(const rev::Image<unsigned char>& firstImage,
		const rev::Image<unsigned char>& secondImage,
		const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry,
		rev::Image<unsigned short>& costImage);

private:
	void allocateDataBuffer();
	void calcTopRowCost(const rev::Image<short>& firstHorizontalSobelImage,
		const rev::Image<short>& firstVerticalSobelImage,
		const rev::Image<short>& secondHorizontalSobelImage,
		const rev::Image<short>& secondVerticalSobelImage,
		const unsigned char* capTable,
		const rev::Image<int>& firstCensusImage,
		const rev::Image<int>& secondCensusImage,
		const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry,
		unsigned short* costImageRow);
	void calcRowCosts(const rev::Image<short>& firstHorizontalSobelImage,
		const rev::Image<short>& firstVerticalSobelImage,
		const rev::Image<short>& secondHorizontalSobelImage,
		const rev::Image<short>& secondVerticalSobelImage,
		const unsigned char* capTable,
		const rev::Image<int>& firstCensusImage,
		const rev::Image<int>& secondCensusImage,
		const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry,
		unsigned short* costImageRow, const int widthStepCost);

	void calcPixelwiseCost(const rev::Image<short>& firstHorizontalSobelImage,
		const rev::Image<short>& firstVerticalSobelImage,
		const rev::Image<short>& secondHorizontalSobelImage,
		const rev::Image<short>& secondVerticalSobelImage,
		const unsigned char* capTable,
		const rev::Image<int>& firstCensusImage,
		const rev::Image<int>& secondCensusImage,
		const int y,
		const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry);
	double directionalSobel(const rev::Image<short>& horizontalSobelImage,
		const rev::Image<short>& verticalSobelImage,
		const double epipolarDirectionX,
		const double epipolarDirectionY,
		const int x, const int y) const;
	int subpixelInterpolatedCappedSobel(const rev::Image<short>& horizontalSobelImage,
		const rev::Image<short>& verticalSobelImage,
		const unsigned char* capTable,
		const double epipolarDirectionX,
		const double epipolarDirectionY,
		const double x, const double y) const;


	// Parameters
	double maxVZRatio_;
	int discretizationTotal_;
	int sobelCapValue_;
	int censusWindowRadius_;
	double censusWeightFactor_;
	int aggregationWindowRadius_;

	// Image size
	int width_;
	int height_;
	// Data buffer
	rev::AlignedMemory<unsigned char> pixelwiseCostRow_;
	rev::AlignedMemory<unsigned short> rowAggregatedCost_;
};
