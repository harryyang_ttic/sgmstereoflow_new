FILE(REMOVE_RECURSE
  "CMakeFiles/smoothfit.dir/src/stereoflow/smoothfit/Smoothfit.o"
  "CMakeFiles/smoothfit.dir/src/stereoflow/smoothfit/SegmentConfiguration.o"
  "CMakeFiles/smoothfit.dir/src/stereoflow/smoothfit/interpolateDisparityImage.o"
  "CMakeFiles/smoothfit.dir/src/stereoflow/smoothfit/smoothfit_main.o"
  "CMakeFiles/smoothfit.dir/src/stereoflow/smoothfit/SupportPoint.o"
  "CMakeFiles/smoothfit.dir/src/stereoflow/smoothfit/Segment.o"
  "CMakeFiles/smoothfit.dir/src/common/CameraMotion.o"
  "CMakeFiles/smoothfit.dir/src/common/CalibratedEpipolarFlowGeometry.o"
  "bin/smoothfit.pdb"
  "bin/smoothfit"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang CXX)
  INCLUDE(CMakeFiles/smoothfit.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
