#ifndef MATCHED_POINT_H
#define MATCHED_POINT_H

#include <revlib.h>

class MatchedPoint {
public:
    MatchedPoint() : firstX_(-1), firstY_(-1), secondX_(-1), secondY_(-1) {}
    MatchedPoint(const double firstX, const double firstY,
                 const double secondX, const double secondY)
        : firstX_(firstX), firstY_(firstY), secondX_(secondX), secondY_(secondY) {}
    MatchedPoint(const rev::Keypoint& firstKeypoint, const rev::Keypoint& secondKeypoint)
        : firstX_(firstKeypoint.x()), firstY_(firstKeypoint.y()),
          secondX_(secondKeypoint.x()), secondY_(secondKeypoint.y()) {}
    
    void set(const double firstX, const double firstY,
             const double secondX, const double secondY)
    {
        firstX_ = firstX;  firstY_ = firstY;
        secondX_ = secondX;  secondY_ = secondY;
    }
    void set(const rev::Keypoint& firstKeypoint, const rev::Keypoint& secondKeypoint) {
        firstX_ = firstKeypoint.x();  firstY_ = firstKeypoint.y();
        secondX_ = secondKeypoint.x();  secondY_ = secondKeypoint.y();
    }
    
    double firstX() const { return firstX_; }
    double firstY() const { return firstY_; }
    double secondX() const { return secondX_; }
    double secondY() const { return secondY_; }
    
private:
    double firstX_;
    double firstY_;
    double secondX_;
    double secondY_;
};

#endif
