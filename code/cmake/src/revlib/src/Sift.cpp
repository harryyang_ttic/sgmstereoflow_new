#include "Sift.h"
#include <fstream>
#include <sstream>
#include <cmath>
#include <emmintrin.h>
#include "Mathematics.h"
#include "ImageProcessing.h"

namespace rev {
    
const int REVLIB_SIFT_DEFAULT_OCTAVE_TOTAL = 0;
const int REVLIB_SIFT_DEFAULT_LEVEL_TOTAL = 3;
const int REVLIB_SIFT_DEFAULT_MIN_OCTAVE_INDEX = -1;
const double REVLIB_SIFT_DEFAULT_PEAK_THRESHOLD = 0.0;
const double REVLIB_SIFT_DEFAULT_EDGE_THRESHOLD = 10.0;
const double REVLIB_SIFT_DEFAULT_NORM_THRESHOLD = 0.0;
const double REVLIB_SIFT_DEFAULT_MAGNIFICATION_FACTOR = 3.0;
const double REVLIB_SIFT_DEFAULT_WINDOW_SIZE = 2.0;
    
const int expTableSize = 256;
const double expTableMax = 25.0;
std::vector<double> expTable;
    
void initializeFastExpTable() {
    expTable.resize(expTableSize + 1);
    for (int k = 0; k <= expTableSize; ++k) {
        expTable[k] = exp(-static_cast<double>(k)*(expTableMax/expTableSize));
    }
}
    
inline double fastExp(const double x) {
    if (x > expTableMax) return 0.0;
    
    double normalizedX = x*expTableSize/expTableMax;
    int normalizedXInt = floor(normalizedX);
    double normalizedXSub = normalizedX - normalizedXInt;
    
    double expValueA = expTable[normalizedXInt];
    double expValueB = expTable[normalizedXInt + 1];
    
    return expValueA + normalizedXSub*(expValueB - expValueA);
}
        
    
Sift::Sift() : width_(0), height_(0),
               octaveTotal_(REVLIB_SIFT_DEFAULT_OCTAVE_TOTAL),
               minOctaveIndex_(REVLIB_SIFT_DEFAULT_MIN_OCTAVE_INDEX),
               levelTotal_(REVLIB_SIFT_DEFAULT_LEVEL_TOTAL),
               peakThreshold_(REVLIB_SIFT_DEFAULT_PEAK_THRESHOLD),
               edgeThreshold_(REVLIB_SIFT_DEFAULT_EDGE_THRESHOLD),
               normThreshold_(REVLIB_SIFT_DEFAULT_NORM_THRESHOLD),
               magnificationFactor_(REVLIB_SIFT_DEFAULT_MAGNIFICATION_FACTOR),
               windowSize_(REVLIB_SIFT_DEFAULT_WINDOW_SIZE)
{
    initializeFastExpTable();
}
        
void Sift::setParameter(const int octaveTotal, const int levelTotal, const bool preDoubleSize) {
    octaveTotal_ = octaveTotal;
    if (preDoubleSize) octaveTotal_ = octaveTotal_ + 1;
    if (levelTotal < 1) {
        throw Exception("Sift::setParameter", "the number of levels must be larger than zero");
    }
    minOctaveIndex_ = preDoubleSize ? -1 : 0;
    levelTotal_ = levelTotal;
}
    
void Sift::setThreshold(const double peakThreshold, const double edgeThreshold, const double normThreshold) {
    peakThreshold_ = peakThreshold;
    edgeThreshold_ = edgeThreshold;
    normThreshold_ = normThreshold;
}
    
void Sift::detect(const Image<unsigned char>& inputImage,
                  std::vector<Keypoint>& keypoints,
                  std::vector< std::vector<unsigned char> >& keypointDescriptors)
{
    setParameterAll(inputImage);
    
    keypoints.clear();
    keypointDescriptors.clear();
    for (currentOctaveIndex_ = minOctaveIndex_; currentOctaveIndex_ < octaveTotal_; ++currentOctaveIndex_) {
        if (currentOctaveIndex_ == minOctaveIndex_) {
            Image<float> floatImage;
            convertImageUCharToFloat(inputImage, floatImage);
            buildFirstGaussianOctave(floatImage);
        } else {
            buildGaussianOctave();
        }
        buildDoGOctave();
        buildGradientOctave();
        
        std::vector<SiftKeypoint> siftKeypoints;
        detectSiftKeypoint(siftKeypoints);
        
        computeKeypointOrientation(siftKeypoints);
        
        computeKeypointDescriptor(siftKeypoints, keypoints, keypointDescriptors);
    }
}
    
void Sift::computeKeypointDescriptor(const Image<unsigned char>& inputImage,
                                     const std::vector<Keypoint>& keypoints,
                                     std::vector< std::vector<unsigned char> >& keypointDescriptors)
{
    setParameterAll(inputImage);
    
    int keypointTotal = static_cast<int>(keypoints.size());
    keypointDescriptors.resize(keypointTotal);
    for (currentOctaveIndex_ = minOctaveIndex_; currentOctaveIndex_ < octaveTotal_; ++currentOctaveIndex_) {
        if (currentOctaveIndex_ == minOctaveIndex_) {
            Image<float> floatImage;
            convertImageUCharToFloat(inputImage, floatImage);
            buildFirstGaussianOctave(floatImage);
        } else {
            buildGaussianOctave();
        }
        buildDoGOctave();
        buildGradientOctave();
        
        for (int keypointIndex = 0; keypointIndex < keypointTotal; ++keypointIndex) {
            SiftKeypoint siftKeypoint;
            bool converted = convertToSiftKeypoint(keypoints[keypointIndex], siftKeypoint);
            if (!converted) continue;
            
            computeDescriptor(siftKeypoint, 0, keypointDescriptors[keypointIndex]);
        }
    }
}
    
    
void Sift::setParameterAll(const Image<unsigned char>& inputImage) {
    width_ = inputImage.width();
    height_ = inputImage.height();
    
    if (octaveTotal_ < 1) {
        int smallerSize = width_ > height_ ? height_ : width_;
        octaveTotal_ = floor(log2(smallerSize)) - 3;
        if (octaveTotal_ < 1) octaveTotal_ = 1;
    }
    gaussianLevelTotal_ = levelTotal_ + 3;
    
    gaussianOctave_.resize(gaussianLevelTotal_);
    dogOctave_.resize(gaussianLevelTotal_ - 1);
    gradientMagnitudeOctave_.resize(levelTotal_);
    gradientOrientationOctave_.resize(levelTotal_);
    
    nomialSigma_ = 0.5;
    levelSigma_ = pow(2.0, 1.0/levelTotal_);
    baseSigma_ = 1.6*levelSigma_;
    deltaSigma_ = baseSigma_*sqrt(1.0 - 1.0/(levelSigma_*levelSigma_));
}
    
void Sift::buildFirstGaussianOctave(const Image<float>& inputImage) {
    Image<float> initialImage;
    if (minOctaveIndex_ < 0) {
        upsampleImage(inputImage, initialImage);
    } else {
        initialImage = inputImage;
    }
    
    double sigmaA = baseSigma_/levelSigma_;
    double sigmaB = nomialSigma_*pow(2.0, -minOctaveIndex_);
    if (sigmaA > sigmaB) {
        double firstSigma = sqrt(sigmaA*sigmaA - sigmaB*sigmaB);
        gaussianSmooth(initialImage, firstSigma, gaussianOctave_[0]);
    }
    octaveWidth_ = gaussianOctave_[0].width();
    octaveHeight_ = gaussianOctave_[0].height();
    
    for (int levelIndex = 1; levelIndex < gaussianLevelTotal_; ++levelIndex) {
        double gaussianSigma = deltaSigma_*pow(levelSigma_, levelIndex - 1);
        gaussianSmooth(gaussianOctave_[levelIndex - 1], gaussianSigma, gaussianOctave_[levelIndex]);
    }
}
    
void Sift::buildGaussianOctave() {
    downsampleImage(gaussianOctave_[levelTotal_], gaussianOctave_[0]);
    octaveWidth_ = gaussianOctave_[0].width();
    octaveHeight_ = gaussianOctave_[0].height();

    for (int levelIndex = 1; levelIndex < gaussianLevelTotal_; ++levelIndex) {
        double gaussianSigma = deltaSigma_*pow(levelSigma_, levelIndex - 1);
        gaussianSmooth(gaussianOctave_[levelIndex - 1], gaussianSigma, gaussianOctave_[levelIndex]);
    }
}
    
void Sift::buildDoGOctave() {
    int widthStep = gaussianOctave_[0].widthStep();
    
    for (int levelIndex = 0; levelIndex < gaussianLevelTotal_ - 1; ++levelIndex) {
        float* gaussianUpImageData = gaussianOctave_[levelIndex + 1].dataPointer();
        float* gaussianDownImageData = gaussianOctave_[levelIndex].dataPointer();
        
        dogOctave_[levelIndex].resize(octaveWidth_, octaveHeight_, 1);
        float* dogImageData = dogOctave_[levelIndex].dataPointer();
        
        for (int y = 0; y < octaveHeight_; ++y) {
            float* gaussianUpRowData = gaussianUpImageData + widthStep*y;
            float* gaussianDownRowData = gaussianDownImageData + widthStep*y;
            float* dogRowData = dogImageData + widthStep*y;
            
            for (int x = 0; x < octaveWidth_; x += 4) {
                __m128 regDifference = _mm_sub_ps(_mm_load_ps(gaussianUpRowData + x),
                                                  _mm_load_ps(gaussianDownRowData + x));
                _mm_store_ps(dogRowData + x, regDifference);
            }
        }
    }
}
    
void Sift::buildGradientOctave() {
    int widthStep = dogOctave_[0].widthStep();
    for (int levelIndex = 0; levelIndex < levelTotal_; ++levelIndex) {
        gradientMagnitudeOctave_[levelIndex].resize(octaveWidth_, octaveHeight_, 1);
        gradientOrientationOctave_[levelIndex].resize(octaveWidth_, octaveHeight_, 1);
        
        float* gaussianImageData = gaussianOctave_[levelIndex + 1].dataPointer();
        float* magnitudeImageData = gradientMagnitudeOctave_[levelIndex].dataPointer();
        float* orientationImageData = gradientOrientationOctave_[levelIndex].dataPointer();
        
        // First row
        float* gaussianRowData = gaussianImageData;
        float* magnitudeRowData = magnitudeImageData;
        float* orientationRowData = orientationImageData;
        // First pixel
        float dx = gaussianRowData[1] - gaussianRowData[0];
        float dy = gaussianRowData[widthStep] - gaussianRowData[0];
        magnitudeRowData[0] = fastSqrtf(dx*dx + dy*dy);
        orientationRowData[0] = mod2Pif(fastAtan2f(dy, dx) + 2*REV_PI);
        // Middle pixels
        for (int x = 1; x < octaveWidth_ - 1; ++x) {
            dx = 0.5*(gaussianRowData[x + 1] - gaussianRowData[x - 1]);
            dy = gaussianRowData[widthStep + x] - gaussianRowData[x];
            magnitudeRowData[x] = fastSqrtf(dx*dx + dy*dy);
            orientationRowData[x] = mod2Pif(fastAtan2f(dy, dx) + 2*REV_PI);
        }
        // Last pixel
        dx = gaussianRowData[octaveWidth_ - 1] - gaussianRowData[octaveWidth_ - 2];
        dy = gaussianRowData[widthStep + octaveWidth_ - 1] - gaussianRowData[octaveWidth_ - 1];
        magnitudeRowData[octaveWidth_ - 1] = fastSqrtf(dx*dx + dy*dy);
        orientationRowData[octaveWidth_ - 1] = mod2Pif(fastAtan2f(dy, dx) + 2*REV_PI);
        
        // Middle rows
        for (int y = 1; y < octaveHeight_ - 1; ++y) {
            gaussianRowData = gaussianImageData + widthStep*y;
            magnitudeRowData = magnitudeImageData + widthStep*y;
            orientationRowData = orientationImageData + widthStep*y;
            // First pixel
            dx = gaussianRowData[1] - gaussianRowData[0];
            dy = 0.5*(gaussianRowData[widthStep] - gaussianRowData[-widthStep]);
            magnitudeRowData[0] = fastSqrtf(dx*dx + dy*dy);
            orientationRowData[0] = mod2Pif(fastAtan2f(dy, dx) + 2*REV_PI);
            // Middle pixels
            for (int x = 1; x < octaveWidth_ - 1; ++x) {
                dx = 0.5*(gaussianRowData[x + 1] - gaussianRowData[x - 1]);
                dy = 0.5*(gaussianRowData[widthStep + x] - gaussianRowData[-widthStep + x]);
                magnitudeRowData[x] = fastSqrtf(dx*dx + dy*dy);
                orientationRowData[x] = mod2Pif(fastAtan2f(dy, dx) + 2*REV_PI);
            }
            // Last pixel
            dx = gaussianRowData[octaveWidth_ - 1] - gaussianRowData[octaveWidth_ - 2];
            dy = 0.5*(gaussianRowData[widthStep + octaveWidth_ - 1] - gaussianRowData[-widthStep + octaveWidth_ - 1]);
            magnitudeRowData[octaveWidth_ - 1] = fastSqrtf(dx*dx + dy*dy);
            orientationRowData[octaveWidth_ - 1] = mod2Pif(fastAtan2f(dy, dx) + 2*REV_PI);
        }
        
        // Last row
        gaussianRowData = gaussianImageData + widthStep*(octaveHeight_ - 1);
        magnitudeRowData = magnitudeImageData + widthStep*(octaveHeight_ - 1);
        orientationRowData = orientationImageData + widthStep*(octaveHeight_ - 1);
        // First pixel
        dx = gaussianRowData[1] - gaussianRowData[0];
        dy = gaussianRowData[0] - gaussianRowData[-widthStep];
        magnitudeRowData[0] = fastSqrtf(dx*dx + dy*dy);
        orientationRowData[0] = mod2Pif(fastAtan2f(dy, dx) + 2*REV_PI);
        // Middle pixels
        for (int x = 1; x < octaveWidth_ - 1; ++x) {
            dx = 0.5*(gaussianRowData[x + 1] - gaussianRowData[x - 1]);
            dy = gaussianRowData[x] - gaussianRowData[-widthStep + x];
            magnitudeRowData[x] = fastSqrtf(dx*dx + dy*dy);
            orientationRowData[x] = mod2Pif(fastAtan2f(dy, dx) + 2*REV_PI);
        }
        // Last pixel
        dx = gaussianRowData[octaveWidth_ - 1] - gaussianRowData[octaveWidth_ - 2];
        dy = gaussianRowData[octaveWidth_ - 1] - gaussianRowData[-widthStep + octaveWidth_ - 1];
        magnitudeRowData[octaveWidth_ - 1] = fastSqrtf(dx*dx + dy*dy);
        orientationRowData[octaveWidth_ - 1] = mod2Pif(fastAtan2f(dy, dx) + 2*REV_PI);
    }
}
    
void Sift::detectSiftKeypoint(std::vector<SiftKeypoint>& siftKeypoints) const {
    for (int levelIndex = 1; levelIndex < gaussianLevelTotal_ - 2; ++levelIndex) {
        for (int y = 1; y < octaveHeight_ - 1; ++y) {
            for (int x = 1; x < octaveWidth_ - 1; ++x) {
                if (isLocalMaxima(x, y, levelIndex)) {
                    refineLocalMaxima(x, y, levelIndex, siftKeypoints);
                }
            }
        }
    }
}
    
void Sift::computeKeypointOrientation(std::vector<SiftKeypoint>& siftKeypoints) const {
    int keypointTotal = static_cast<int>(siftKeypoints.size());
    for (int i = 0; i < keypointTotal; ++i) {
        computeOrientation(siftKeypoints[i]);
    }
}
    
void Sift::computeKeypointDescriptor(const std::vector<SiftKeypoint>& siftKeypoints,
                                     std::vector<Keypoint>& keypoints,
                                     std::vector< std::vector<unsigned char> >& keypointDescriptors) const
{
    int siftKeypointTotal = static_cast<int>(siftKeypoints.size());
    for (int pointIndex = 0; pointIndex < siftKeypointTotal; ++pointIndex) {
        int orientationTotal = static_cast<int>(siftKeypoints[pointIndex].orientations.size());
        for (int orientationIndex = 0; orientationIndex < orientationTotal; ++orientationIndex) {
            std::vector<unsigned char> siftDescriptor;
            bool succeeded = computeDescriptor(siftKeypoints[pointIndex], orientationIndex, siftDescriptor);
            
            if (succeeded) {
                Keypoint newKeypoint;
                newKeypoint.setPosition(siftKeypoints[pointIndex].x, siftKeypoints[pointIndex].y);
                newKeypoint.setScale(siftKeypoints[pointIndex].scale);
                newKeypoint.setOrientation(siftKeypoints[pointIndex].orientations[orientationIndex]);
                keypoints.push_back(newKeypoint);
            
                keypointDescriptors.push_back(siftDescriptor);
            }
        }
    }
}
    
bool Sift::convertToSiftKeypoint(const Keypoint& keypoint, SiftKeypoint& siftKeypoint) const {
    siftKeypoint.x = keypoint.x();
    siftKeypoint.y = keypoint.y();
    siftKeypoint.scale = keypoint.scale();
    siftKeypoint.orientations.resize(1);
    siftKeypoint.orientations[0] = keypoint.orientation();
    
    double subLevelIndex = log2(siftKeypoint.scale/baseSigma_/pow(2.0, currentOctaveIndex_))*levelTotal_ + 1;
    if (subLevelIndex < 0 || subLevelIndex > levelTotal_) return false;
    
    double positionScaleFactorInverse = 1.0/pow(2.0, currentOctaveIndex_);
    siftKeypoint.octaveX = static_cast<int>(siftKeypoint.x*positionScaleFactorInverse + 0.5);
    siftKeypoint.octaveY = static_cast<int>(siftKeypoint.y*positionScaleFactorInverse + 0.5);
    siftKeypoint.octaveLevelIndex = static_cast<int>(subLevelIndex + 0.5);
    if (siftKeypoint.octaveLevelIndex < 1) siftKeypoint.octaveLevelIndex = 1;
    siftKeypoint.octaveIndex = currentOctaveIndex_;
    
    return true;
}
    

void Sift::upsampleImage(const Image<float>& sourceImage, Image<float>& upsampledImage) const {
    Image<float> upsampledHeightImage;
    upsampleRowTranspose(sourceImage, upsampledHeightImage);
    upsampleRowTranspose(upsampledHeightImage, upsampledImage);
}
    
void Sift::upsampleRowTranspose(const Image<float>& sourceImage, Image<float>& upsampledHeightImage) const {
    int width = sourceImage.width();
    int height = sourceImage.height();
    
    upsampledHeightImage.resize(height, 2*width, 1);
    for (int y = 0; y < height; ++y) {
        float firstPixelValue, secondPixelValue;
        firstPixelValue = sourceImage(0, y);
        for (int x = 0; x < width - 1; ++x) {
            secondPixelValue = sourceImage(x + 1, y);
            upsampledHeightImage(y, 2*x) = firstPixelValue;
            upsampledHeightImage(y, 2*x + 1) = 0.5*(firstPixelValue + secondPixelValue);
            firstPixelValue = secondPixelValue;
        }
        upsampledHeightImage(y, 2*width - 2) = secondPixelValue;
        upsampledHeightImage(y, 2*width - 1) = secondPixelValue;
    }
}
    
void Sift::downsampleImage(const Image<float>& sourceImage, Image<float>& downsampledImage) const {
    int width = sourceImage.width();
    int height = sourceImage.height();
    
    int downsampledWidth = floor(width/2);
    int downsampledHeight = floor(height/2);
    downsampledImage.resize(downsampledWidth, downsampledHeight, 1);
    for (int y = 0; y < downsampledHeight; ++y) {
        for (int x = 0; x < downsampledWidth; ++x) {
            downsampledImage(x, y) = sourceImage(2*x, 2*y);
        }
    }
}
    
bool Sift::isLocalMaxima(const int x, const int y, const int levelIndex) const {
    float dogValue = dogOctave_[levelIndex](x, y);
    if ((dogValue > 0.8*peakThreshold_
         
         && dogValue > dogOctave_[levelIndex - 1](x - 1, y - 1)
         && dogValue > dogOctave_[levelIndex - 1](x, y - 1)
         && dogValue > dogOctave_[levelIndex - 1](x + 1, y - 1)
         && dogValue > dogOctave_[levelIndex - 1](x - 1, y)
         && dogValue > dogOctave_[levelIndex - 1](x, y)
         && dogValue > dogOctave_[levelIndex - 1](x + 1, y)
         && dogValue > dogOctave_[levelIndex - 1](x - 1, y + 1)
         && dogValue > dogOctave_[levelIndex - 1](x, y + 1)
         && dogValue > dogOctave_[levelIndex - 1](x + 1, y + 1)
         
         && dogValue > dogOctave_[levelIndex](x - 1, y - 1)
         && dogValue > dogOctave_[levelIndex](x, y - 1)
         && dogValue > dogOctave_[levelIndex](x + 1, y - 1)
         && dogValue > dogOctave_[levelIndex](x - 1, y)
         && dogValue > dogOctave_[levelIndex](x + 1, y)
         && dogValue > dogOctave_[levelIndex](x - 1, y + 1)
         && dogValue > dogOctave_[levelIndex](x, y + 1)
         && dogValue > dogOctave_[levelIndex](x + 1, y + 1)
         
         && dogValue > dogOctave_[levelIndex + 1](x - 1, y - 1)
         && dogValue > dogOctave_[levelIndex + 1](x, y - 1)
         && dogValue > dogOctave_[levelIndex + 1](x + 1, y - 1)
         && dogValue > dogOctave_[levelIndex + 1](x - 1, y)
         && dogValue > dogOctave_[levelIndex + 1](x, y)
         && dogValue > dogOctave_[levelIndex + 1](x + 1, y)
         && dogValue > dogOctave_[levelIndex + 1](x - 1, y + 1)
         && dogValue > dogOctave_[levelIndex + 1](x, y + 1)
         && dogValue > dogOctave_[levelIndex + 1](x + 1, y + 1))
        
        || (dogValue < -0.8*peakThreshold_
            
            && dogValue < dogOctave_[levelIndex - 1](x - 1, y - 1)
            && dogValue < dogOctave_[levelIndex - 1](x, y - 1)
            && dogValue < dogOctave_[levelIndex - 1](x + 1, y - 1)
            && dogValue < dogOctave_[levelIndex - 1](x - 1, y)
            && dogValue < dogOctave_[levelIndex - 1](x, y)
            && dogValue < dogOctave_[levelIndex - 1](x + 1, y)
            && dogValue < dogOctave_[levelIndex - 1](x - 1, y + 1)
            && dogValue < dogOctave_[levelIndex - 1](x, y + 1)
            && dogValue < dogOctave_[levelIndex - 1](x + 1, y + 1)
            
            && dogValue < dogOctave_[levelIndex](x - 1, y - 1)
            && dogValue < dogOctave_[levelIndex](x, y - 1)
            && dogValue < dogOctave_[levelIndex](x + 1, y - 1)
            && dogValue < dogOctave_[levelIndex](x - 1, y)
            && dogValue < dogOctave_[levelIndex](x + 1, y)
            && dogValue < dogOctave_[levelIndex](x - 1, y + 1)
            && dogValue < dogOctave_[levelIndex](x, y + 1)
            && dogValue < dogOctave_[levelIndex](x + 1, y + 1)
            
            && dogValue < dogOctave_[levelIndex + 1](x - 1, y - 1)
            && dogValue < dogOctave_[levelIndex + 1](x, y - 1)
            && dogValue < dogOctave_[levelIndex + 1](x + 1, y - 1)
            && dogValue < dogOctave_[levelIndex + 1](x - 1, y)
            && dogValue < dogOctave_[levelIndex + 1](x, y)
            && dogValue < dogOctave_[levelIndex + 1](x + 1, y)
            && dogValue < dogOctave_[levelIndex + 1](x - 1, y + 1)
            && dogValue < dogOctave_[levelIndex + 1](x, y + 1)
            && dogValue < dogOctave_[levelIndex + 1](x + 1, y + 1)))
    {
        return true;
    }
    return false;
}

void Sift::refineLocalMaxima(const int x, const int y, const int levelIndex,
                             std::vector<SiftKeypoint>& siftKeypoints) const
{
    int refinedX = x;
    int refinedY = y;
    
    double Dx = 0, Dy = 0, Ds = 0, Dxx = 0, Dyy = 0, Dss = 0, Dxy = 0, Dxs = 0, Dys = 0;
    double A[9], b[3];
    
    int dx = 0;
    int dy = 0;
    for (int iterationCount = 0; iterationCount < 5; ++iterationCount) {
        refinedX += dx;
        refinedY += dy;
        
        Dx = 0.5*(dogOctave_[levelIndex](refinedX + 1, refinedY) - dogOctave_[levelIndex](refinedX - 1, refinedY));
        Dy = 0.5*(dogOctave_[levelIndex](refinedX, refinedY + 1) - dogOctave_[levelIndex](refinedX, refinedY - 1));
        Ds = 0.5*(dogOctave_[levelIndex + 1](refinedX, refinedY) - dogOctave_[levelIndex - 1](refinedX, refinedY));
        
        Dxx = dogOctave_[levelIndex](refinedX + 1, refinedY) + dogOctave_[levelIndex](refinedX - 1, refinedY)
            - 2.0*dogOctave_[levelIndex](refinedX, refinedY);
        Dyy = dogOctave_[levelIndex](refinedX, refinedY + 1) + dogOctave_[levelIndex](refinedX, refinedY - 1)
            - 2.0*dogOctave_[levelIndex](refinedX, refinedY);
        Dss = dogOctave_[levelIndex + 1](refinedX, refinedY) + dogOctave_[levelIndex - 1](refinedX, refinedY)
            - 2.0*dogOctave_[levelIndex](refinedX, refinedY);
        
        Dxy = 0.25*(dogOctave_[levelIndex](refinedX + 1, refinedY + 1)
                    + dogOctave_[levelIndex](refinedX - 1, refinedY - 1)
                    - dogOctave_[levelIndex](refinedX - 1, refinedY + 1)
                    - dogOctave_[levelIndex](refinedX + 1, refinedY - 1));
        Dxs = 0.25*(dogOctave_[levelIndex + 1](refinedX + 1, refinedY)
                    + dogOctave_[levelIndex - 1](refinedX - 1, refinedY)
                    - dogOctave_[levelIndex + 1](refinedX - 1, refinedY)
                    - dogOctave_[levelIndex - 1](refinedX + 1, refinedY));
        Dys = 0.25*(dogOctave_[levelIndex + 1](refinedX, refinedY + 1)
                    + dogOctave_[levelIndex - 1](refinedX, refinedY - 1)
                    - dogOctave_[levelIndex + 1](refinedX, refinedY - 1)
                    - dogOctave_[levelIndex - 1](refinedX, refinedY + 1));
        
        A[0] = Dxx;  A[1] = Dxy;  A[2] = Dxs;
        A[3] = Dxy;  A[4] = Dyy;  A[5] = Dys;
        A[6] = Dxs;  A[7] = Dys;  A[8] = Dss;
        
        b[0] = -Dx;
        b[1] = -Dy;
        b[2] = -Ds;
        
        // Gauss elimination
        for (int rowIndex = 0; rowIndex < 3; ++rowIndex) {
            double maxElementValue = 0;
            double maxAbsoluteElementValue = 0;
            int maxColumnIndex = -1;
            for (int columnIndex = rowIndex; columnIndex < 3; ++columnIndex) {
                double elementValue = A[3*rowIndex + columnIndex];
                double absoluteElementValue = fabs(elementValue);
                if (absoluteElementValue > maxAbsoluteElementValue) {
                    maxElementValue = elementValue;
                    maxAbsoluteElementValue = absoluteElementValue;
                    maxColumnIndex = columnIndex;
                }
            }
            
            // if singular, give up
            if (maxAbsoluteElementValue < 1e-10) {
                b[0] = 0;
                b[1] = 0;
                b[2] = 0;
                break;
            }
            
            // Swap
            for (int swapRowIndex = rowIndex; swapRowIndex < 3; ++swapRowIndex) {
                double tmpElementValue = A[3*swapRowIndex + maxColumnIndex];
                A[3*swapRowIndex + maxColumnIndex] = A[3*swapRowIndex + rowIndex];
                A[3*swapRowIndex + rowIndex] = tmpElementValue;
                A[3*swapRowIndex + rowIndex] /= maxElementValue;
            }
            double tmpElementValue = b[rowIndex];
            b[rowIndex] = b[maxColumnIndex];
            b[maxColumnIndex] = tmpElementValue;
            b[rowIndex] /= maxElementValue;
            
            // Elimination
            for (int columnIndex = rowIndex + 1; columnIndex < 3; ++columnIndex) {
                double x = A[3*rowIndex + columnIndex];
                for (int ri = rowIndex; ri < 3; ++ri) {
                    A[3*ri + columnIndex] -= x*A[3*ri + rowIndex];
                }
                b[columnIndex] -= x*b[rowIndex];
            }
        }
        
        // Backward substitution
        for (int rowIndex = 2; rowIndex > 0; --rowIndex) {
            double x = b[rowIndex];
            for (int columnIndex = rowIndex - 1; columnIndex >= 0; --columnIndex) {
                b[columnIndex] -= x*A[3*rowIndex + columnIndex];
            }
        }

        if (b[0] > 0.6 && refinedX < octaveWidth_ - 2) dx = 1;
        else if (b[0] < -0.6 && refinedX > 1) dx = -1;
        else dx = 0;
        
        if (b[1] > 0.6 && refinedY < octaveHeight_ - 2) dy = 1;
        else if (b[1] < -0.6 && refinedY > 1) dy = -1;
        else dy = 0;
        
        if (dx == 0 && dy == 0) break;
    }
    
    // Check threshold and other conditions
    double peakValue = dogOctave_[levelIndex](refinedX, refinedY) + 0.5*(Dx*b[0] + Dy*b[1] + Ds*b[2]);
    double edgeScore = (Dxx + Dyy)*(Dxx + Dyy)/(Dxx*Dyy - Dxy*Dxy);
    double subpixelX = refinedX + b[0];
    double subpixelY = refinedY + b[1];
    double subLevelIndex = levelIndex + b[2];
    
    if (fabs(peakValue) > peakThreshold_
        && edgeScore < (edgeThreshold_ + 1)*(edgeThreshold_ + 1)/edgeThreshold_ && edgeScore >= 0
        && fabs(b[0]) < 1.5 && fabs(b[1]) < 1.5 && fabs(b[2]) < 1.5
        && subpixelX >= 0 && subpixelX <= octaveWidth_ - 1
        && subpixelY >= 0 && subpixelY <= octaveHeight_ - 1
        && subLevelIndex >= 0 && subLevelIndex <= gaussianLevelTotal_ - 1)
    {
        SiftKeypoint newKeypoint;
        newKeypoint.octaveIndex = currentOctaveIndex_;
        newKeypoint.octaveX = refinedX;
        newKeypoint.octaveY = refinedY;
        newKeypoint.octaveLevelIndex = levelIndex;
        double positionScaleFactor = pow(2.0, currentOctaveIndex_);
        newKeypoint.x = static_cast<float>(subpixelX*positionScaleFactor);
        newKeypoint.y = static_cast<float>(subpixelY*positionScaleFactor);
        newKeypoint.levelIndex = static_cast<float>(subLevelIndex);
        newKeypoint.scale = static_cast<float>(baseSigma_*pow(2.0, (subLevelIndex - 1)/levelTotal_)*positionScaleFactor);
        
        siftKeypoints.push_back(newKeypoint);
    }
}
    
void Sift::computeOrientation(SiftKeypoint& siftKeypoint) const {
    siftKeypoint.orientations.clear();
    
    double positionScaleFactorInverse = 1.0/pow(2.0, currentOctaveIndex_);
    double x = siftKeypoint.x*positionScaleFactorInverse;
    double y = siftKeypoint.y*positionScaleFactorInverse;
    double sigma = siftKeypoint.scale*positionScaleFactorInverse;

    const double windowFactor = 1.5;
    double windowSigma = windowFactor*sigma;
    int windowRadius = floor(3.0*windowSigma);
    if (windowRadius < 1) windowRadius = 1;
    
    int xInt = static_cast<int>(x + 0.5);
    int yInt = static_cast<int>(y + 0.5);
    int levelIndexInt = siftKeypoint.octaveLevelIndex;
    
    if (xInt < 0 || xInt >= octaveWidth_
        || yInt < 0 || yInt >= octaveHeight_
        || levelIndexInt < 1 || levelIndexInt > levelTotal_) return;
    
    int offsetXMin = xInt < windowRadius ? -xInt : -windowRadius;
    int offsetXMax = octaveWidth_ - xInt - 1 < windowRadius ? octaveWidth_ - xInt - 1 : windowRadius;
    int offsetYMin = yInt < windowRadius ? -yInt : -windowRadius;
    int offsetYMax = octaveHeight_ - yInt - 1 < windowRadius ? octaveHeight_ - yInt - 1 : windowRadius;
    
    // Compute orientation histogram
    const int binTotal = 36;
    std::vector<double> orientationHistogram(binTotal, 0);
    for (int offsetY = offsetYMin; offsetY <= offsetYMax; ++offsetY) {
        for (int offsetX = offsetXMin; offsetX <= offsetXMax; ++offsetX) {
            double distanceX = xInt + offsetX - x;
            double distanceY = yInt + offsetY - y;
            double distanceSquare = distanceX*distanceX + distanceY*distanceY;
            if (distanceSquare >= windowRadius*windowRadius + 0.6) continue;
            
            double weightValue = fastExp(distanceSquare/(2*windowSigma*windowSigma));
            double gradientMagnitude = gradientMagnitudeOctave_[levelIndexInt - 1](xInt + offsetX, yInt + offsetY);
            double gradientOrientation = gradientOrientationOctave_[levelIndexInt - 1](xInt + offsetX, yInt + offsetY);

            double binIndex = binTotal*gradientOrientation/(2*REV_PI);
            int binIndexInt = floor(binIndex - 0.5);
            double binIndexSub = binIndex - binIndexInt - 0.5;
            orientationHistogram[(binIndexInt + binTotal)%binTotal] += (1 - binIndexSub)*gradientMagnitude*weightValue;
            orientationHistogram[(binIndexInt + 1)%binTotal] += binIndexSub*gradientMagnitude*weightValue;
        }
    }
    
    // Smooth histogram
    for (int iterationCount = 0; iterationCount < 6; ++iterationCount) {
        double firstValue = orientationHistogram[0];
        double previoutValue = orientationHistogram[binTotal - 1];
        for (int i = 0; i < binTotal - 1; ++i) {
            double newValue = (previoutValue + orientationHistogram[i] + orientationHistogram[(i + 1)%binTotal])/3.0;
            previoutValue = orientationHistogram[i];
            orientationHistogram[i] = newValue;
        }
        orientationHistogram[binTotal - 1] = (previoutValue + orientationHistogram[binTotal - 1] + firstValue)/3.0;
    }
    
    // Maximum value of histogram
    double maxValue = 0;
    for (int i = 0; i < binTotal; ++i) {
        if (orientationHistogram[i] > maxValue) maxValue = orientationHistogram[i];
    }
    
    // Find peaks
    double orientationThreshold = 0.8*maxValue;
    for (int i = 0; i < binTotal; ++i) {
        double currentValue = orientationHistogram[i];
        double previousValue = orientationHistogram[(i - 1 + binTotal)%binTotal];
        double nextValue = orientationHistogram[(i + 1)%binTotal];
        
        if (currentValue > orientationThreshold && currentValue > previousValue && currentValue > nextValue) {
            // Quadratic interpolation
            double interpolateValue = -0.5*(nextValue - previousValue)/(nextValue + previousValue - 2.0*currentValue);
            double interpolatedOrientation = 2*REV_PI*(i + interpolateValue + 0.5)/binTotal;
            
            siftKeypoint.orientations.push_back(interpolatedOrientation);
        }
    }
}
    
bool Sift::computeDescriptor(const SiftKeypoint& siftKeypoint,
                             const int orientationIndex,
                             std::vector<unsigned char>& siftDescriptor) const
{
    const int spatialBinTotal = 4;
    const int orientationBinTotal = 8;
    
    double positionScaleFactorInverse = 1.0/pow(2.0, currentOctaveIndex_);
    double x = siftKeypoint.x*positionScaleFactorInverse;
    double y = siftKeypoint.y*positionScaleFactorInverse;
    double sigma = siftKeypoint.scale*positionScaleFactorInverse;
    double orientation = siftKeypoint.orientations[orientationIndex];
    
    int ix = static_cast<int>(x + 0.5);
    int iy = static_cast<int>(y + 0.5);
    int iLevelIndex = siftKeypoint.octaveLevelIndex;

    double orientationSin = sin(orientation);
    double orientationCos = cos(orientation);
    double spatialSize = magnificationFactor_*sigma + EpsilonDouble;
    int windowRadius = static_cast<int>(sqrt(2.0)*spatialSize*(spatialBinTotal + 1)/2.0 + 0.5);
    
    if (ix < 0 || ix >= octaveWidth_ || iy < 0 || iy >= octaveHeight_
        || iLevelIndex < 1 || iLevelIndex > levelTotal_) return false;
    
    int descriptorSize = spatialBinTotal*spatialBinTotal*orientationBinTotal;
    std::vector<double> descriptorHistogram(descriptorSize);
    for (int i = 0; i < descriptorSize; ++i) descriptorHistogram[i] = 0;
    
    int offsetXMin = ix < windowRadius + 1 ? 1 - ix : -windowRadius;
    int offsetXMax = octaveWidth_ - ix - 2 < windowRadius ? octaveWidth_ - ix - 2 : windowRadius;
    int offsetYMin = iy < windowRadius + 1 ? 1 - iy : -windowRadius;
    int offsetYMax = octaveHeight_ - iy - 2 < windowRadius ? octaveHeight_ - iy - 2 : windowRadius;
    
    for (int offsetY = offsetYMin; offsetY <= offsetYMax; ++offsetY) {
        for (int offsetX = offsetXMin; offsetX <= offsetXMax; ++offsetX) {
            double pixelMagnitude = gradientMagnitudeOctave_[iLevelIndex - 1](ix + offsetX, iy + offsetY);
            double pixelOrientation = gradientOrientationOctave_[iLevelIndex - 1](ix + offsetX, iy + offsetY);
            double theta = mod2Pif(pixelOrientation - orientation);
            
            double distanceX = ix + offsetX - x;
            double distanceY = iy + offsetY - y;
            
            double normalizedX = (orientationCos*distanceX + orientationSin*distanceY)/spatialSize;
            double normalizedY = (-orientationSin*distanceX + orientationCos*distanceY)/spatialSize;
            double normalizedOrientation = orientationBinTotal*theta/(2*REV_PI);
            
            double gaussianWeight = fastExp((normalizedX*normalizedX + normalizedY*normalizedY)/(2.0*windowSize_*windowSize_));
            
            int binX = floor(normalizedX - 0.5);
            int binY = floor(normalizedY - 0.5);
            int binO = floor(normalizedOrientation);
            double binXSub = normalizedX - binX - 0.5;
            double binYSub = normalizedY - binY - 0.5;
            double binOSub = normalizedOrientation - binO;
            
            for (int offsetBinX = 0; offsetBinX < 2; ++offsetBinX) {
                for (int offsetBinY = 0; offsetBinY < 2; ++offsetBinY) {
                    for (int offsetBinO = 0; offsetBinO < 2; ++offsetBinO) {
                        if (binX + offsetBinX >= -(spatialBinTotal/2)
                            && binX + offsetBinX < spatialBinTotal/2
                            && binY + offsetBinY >= -(spatialBinTotal/2)
                            && binY + offsetBinY < spatialBinTotal/2)
                        {
                            double weightedValue = gaussianWeight*pixelMagnitude
                                *fabs(1 - offsetBinX - binXSub)
                                *fabs(1 - offsetBinY - binYSub)
                                *fabs(1 - offsetBinO - binOSub);
                            
                            descriptorHistogram[orientationBinTotal*spatialBinTotal*(spatialBinTotal/2 + binY + offsetBinY)
                                                + orientationBinTotal*(spatialBinTotal/2 + binX + offsetBinX)
                                                + (binO + offsetBinO)%orientationBinTotal] += weightedValue;
                        }
                    }
                }
            }
        }
    }
    
    // Normalize
    double normL2 = normalizeHistogram(descriptorHistogram);
    if (normL2 < normThreshold_) return false;
    // Truncate
    const double truncateThreshold = 0.2;
    for (int binIndex = 0; binIndex < descriptorSize; ++binIndex) {
        if (descriptorHistogram[binIndex] > truncateThreshold) {
            descriptorHistogram[binIndex] = truncateThreshold;
        }
    }
    normalizeHistogram(descriptorHistogram);
    
    // Convert to unsigned char
    siftDescriptor.resize(descriptorSize);
    for (int binIndex = 0; binIndex < descriptorSize; ++binIndex) {
        siftDescriptor[binIndex] = static_cast<unsigned char>(512.0*descriptorHistogram[binIndex]);
    }
    
    return true;
}
    
double Sift::normalizeHistogram(std::vector<double>& histogram) const {
    int histogramSize = static_cast<int>(histogram.size());
    double normL2 = 0.0;
    for (int i = 0; i < histogramSize; ++i) {
        normL2 += histogram[i]*histogram[i];
    }
    normL2 = fastSqrt(normL2) + EpsilonDouble;
    
    for (int i = 0; i < histogramSize; ++i) {
        histogram[i] /= normL2;
    }
    
    return normL2;
}
    
    
void readSiftFile(const std::string filename,
                  std::vector<Keypoint>& keypoints,
                  std::vector< std::vector<unsigned char> >& descriptors)
{
    std::ifstream siftFileStream(filename.c_str(), std::ios_base::in);
    if (siftFileStream.fail()) {
        std::stringstream errorMessage;
        errorMessage << "can't open file (" << filename << ")";
        throw Exception("readSiftFile", errorMessage.str());
    }
    
    keypoints.clear();
    descriptors.clear();
    while (siftFileStream.peek() != -1) {
        double x, y, scale, orientation;
        siftFileStream >> x;
        siftFileStream >> y;
        siftFileStream >> scale;
        siftFileStream >> orientation;
        siftFileStream.get();
        std::vector<unsigned char> featureDescriptor;
        while (siftFileStream.peek() != '\n') {
            int element;
            siftFileStream >> element;
            siftFileStream.get();
            featureDescriptor.push_back(static_cast<unsigned char>(element));
        }
        siftFileStream.get();
        
        Keypoint newKepoint(x, y);
        newKepoint.setScale(scale);
        newKepoint.setOrientation(orientation);
        keypoints.push_back(newKepoint);
        descriptors.push_back(featureDescriptor);
    }
    
    siftFileStream.close();
}

void writeSiftFile(const std::string filename,
                   const std::vector<Keypoint>& keypoints,
                   const std::vector< std::vector<unsigned char> >& descriptors)
{
    int keypointTotal = static_cast<int>(keypoints.size());
    if (keypointTotal == 0) {
        throw Exception("writeSiftFile", "no keypoint");
    }
    if (descriptors.size() != keypointTotal) {
        throw Exception("writeSiftFile", "the numbers of keypoints are different between keypoints and descriptors");
    }
    int dimensionTotal = static_cast<int>(descriptors[0].size());
    
    std::ofstream siftFileStream(filename.c_str(), std::ios_base::out);
    if (siftFileStream.fail()) {
        std::stringstream errorMessage;
        errorMessage << "can't open file (" << filename << ")";
        throw Exception("writeSiftFile", errorMessage.str());
    }

    for (int keypointIndex = 0; keypointIndex < keypointTotal; ++keypointIndex) {
        siftFileStream << keypoints[keypointIndex].x() << " " << keypoints[keypointIndex].y() << " ";
        siftFileStream << keypoints[keypointIndex].scale() << " " << keypoints[keypointIndex].orientation() << " ";
        for (int d = 0; d < dimensionTotal; ++d) {
            siftFileStream << static_cast<int>(descriptors[keypointIndex][d]) << " ";
        }
        siftFileStream << std::endl;
    }
    
    siftFileStream.close();
}

}