#include<iostream>
#include<fstream>
#include<vector>
#include<sstream>
#include<cmath>
#include "opencv2/core/core_c.h"
#include "opencv2/core/core.hpp"
#include "opencv2/flann/miniflann.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/video/video.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/ml/ml.hpp"
#include "opencv2/highgui/highgui_c.h"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/contrib/contrib.hpp"

using namespace std;
using namespace cv;
int main()
{
    ifstream laser("laser.txt"),ray("ray.txt");
    string line1,line2;
    vector<vector<double> > lasero,rayo;
    while(getline(laser,line1) && getline(ray,line2))
    {
        istringstream is(line1),is2(line2);
        vector<double> tmp1(3),tmp2(3);
        is>>tmp1[0]>>tmp1[1]>>tmp1[2];
        is2>>tmp2[0]>>tmp2[1]>>tmp2[2];
        bool flag=false;
        for(int i=0;i<3;i++)
        {
            if(fabs(tmp1[i]-tmp2[i])>20)
            {
                flag=true;
                break;
            }
        }
        if(flag)
        {
            lasero.push_back(tmp1);
            rayo.push_back(tmp2);
        }
    }
    ofstream laserof("laserof.txt"),rayof("rayof.txt"),totalo("totalo.txt");
    for(int i=0;i<lasero.size();i++)
    {
        laserof<<lasero[i][0]<<" "<<lasero[i][1]<<" "<<lasero[i][2]<<endl;
        rayof<<rayo[i][0]<<" "<<rayo[i][1]<<" "<<rayo[i][2]<<endl;
        totalo<<lasero[i][0]<<" "<<lasero[i][1]<<" "<<lasero[i][2]<<endl;
        totalo<<rayo[i][0]<<" "<<rayo[i][1]<<" "<<rayo[i][2]<<endl;
        totalo<<endl;
    }
    laser.close();
    ray.close();
    laserof.close();
    rayof.close();
    totalo.close();
    //get the image
    double fx=1019.304708045163, fy=1019.304708045163,ox=789.3798370361328,oy=612.2119750976562;
    ofstream imageo("imageo.txt");
    for(int i=0;i<rayo.size();i++)
    {
        int x,y;
        double X=rayo[i][0],Y=rayo[i][1],Z=rayo[i][2];
        x=round(fx*X/Z+ox)-1;
        y=round(fy*Y/Z+oy)-1;
        imageo<<x<<" "<<y<<endl;
    }
    imageo.close();

    Mat img=imread("000200_seg_disparity.png");
    int height=img.rows,width=img.cols;
    Mat res(height,width,CV_8UC3);
    for(int h=0;h<height;h++)
    {
        for(int w=0;w<width;w++)
        {
            res.at<uchar>(h,3*w)=255;
            res.at<uchar>(h,3*w+1)=255;
            res.at<uchar>(h,3*w+2)=255;
        }
    }
    for(int i=0;i<rayo.size();i++)
    {
         int x,y;
        double X=rayo[i][0],Y=rayo[i][1],Z=rayo[i][2];
        x=round(fx*X/Z+ox)-1;
        y=round(fy*Y/Z+oy)-1;
        if(x>=0 && x<width && y>=0 && y<height)
        {
            res.at<uchar>(y,3*x)=0;
            res.at<uchar>(y,3*x+1)=0;
            res.at<uchar>(y,3*x+2)=255;
        }
    }
    imwrite("imres.png",res);
    return(0);
}
