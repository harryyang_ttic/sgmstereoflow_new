#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>

#include "dirent.h"
#include "string.h"
#include <float.h>
#include <limits.h>
#include <queue>
#include <fstream>
#include <iostream>
#include <revlib.h>
#include <cmath>
#include <algorithm>

#define EPS 1e-6

using namespace boost::numeric::ublas;

std::vector<std::vector<vector<float> > > findSegment(matrix<float> &seg);
int findNearestSegment(vector<float> &uv, std::vector<std::vector<vector<float> > > &segment, int segId);

std::vector<vector<float> > computeBoundingBox(matrix<float> &seg);
int findNearestSegment_approx(vector<float> &uv, std::vector<vector<float> > &bb, int segId);

int findNearestSegment(vector<float> &uv, matrix<float> &seg, int segId, int *distmap, int thres);
int findNearest(std::queue<std::pair<int, int> > &queue, int i, matrix<float> &seg, int *distmap, int m, int n, int thres);

std::vector<vector<float> > parseVelodynefile(const char *Velodynefile);
std::vector<vector<float> > parsePlanefile(const char *planefile);
matrix<float> loadSegfile(const char *segfile);

int main(int argc, char *argv[]) {

    matrix<float> M_v_c(4, 4);

    M_v_c(0, 0) = -0.00809358404391232; //0.017288890273246; //0.014848627295710; //
    M_v_c(0, 1) = -0.999967572308700; //-0.977755180267978; //-0.977730694647836; //
    M_v_c(0, 2) = -0.00237435960422023; //0.009995524883623; //0.011637154339402; //
    M_v_c(0, 3) = 0.281037177488261; //0.260235693759176; //0.691890693687476; //
    M_v_c(1, 0) = -0.0105632042466814; //0.035254139316351; //0.039367572511361; //
    M_v_c(1, 1) = 0.00246090913771785; //-0.009795009285375; //-0.011508181981677; //
    M_v_c(1, 2) = -0.999945693119595; //-0.979831170416624; //-0.979692883469085; //
    M_v_c(1, 3) = -0.0888997514524071; //-0.353414820793534; //-0.252964713320660; //
    M_v_c(2, 0) = 0.999913274796053; //0.999769808489600; //0.999666128869627; //
    M_v_c(2, 1) = -0.00806227990468220; //-0.007866308970000; //-0.010225143996206; //
    M_v_c(2, 2) = -0.0105744138392157; //0.020083138672400; //0.024236574346239; //
    M_v_c(2, 3) = -0.694856368943206; //-0.636309222218800; //-0.955419412413999; //
    M_v_c(3, 0) = 0;
    M_v_c(3, 1) = 0;
    M_v_c(3, 2) = 0;
    M_v_c(3, 3) = 1;

    matrix<float> M_Proj(3, 3);
    M_Proj(0, 0) = 1019.305; //1054.2;
    M_Proj(0, 1) = 0.0;
    M_Proj(0, 2) = 789.380; //769.264;
    M_Proj(1, 0) = 0.0;
    M_Proj(1, 1) = 1019.305; //1055.0;
    M_Proj(1, 2) = 612.212; //604.039;
    M_Proj(2, 0) = 0.0;
    M_Proj(2, 1) = 0.0;
    M_Proj(2, 2) = 1.0;


    std::ofstream myfile("harry_info.txt");
	/*matrix<float> R_rect(3,3);
	R_rect(0,0) = 9.999239e-01;
	R_rect(0,1) = 9.837760e-03;
	R_rect(0,2) = -7.445048e-03;
	R_rect(1,0) = -9.869795e-03;
	R_rect(1,1) = 9.999421e-01;
	R_rect(1,2) = -4.278459e-03;
	R_rect(2,0) = 7.402527e-03;
	R_rect(2,1) = 4.351614e-03;
	R_rect(2,2) = 9.999631e-01;*/

    vector<float> origin(4);
    origin(0) = 0;
    origin(1) = 0;
    origin(2) = 0;
    origin(3) = 1;
    origin = prod(M_v_c, origin);
    vector<float> ray_ori(3);
    ray_ori(0) = origin(0);
    ray_ori(1) = origin(1);
    ray_ori(2) = origin(2);
	//ray_ori = prod(R_rect, ray_ori);
	myfile<<"ray origin: "<< ray_ori(0) << " " << ray_ori(1) << " " << ray_ori(2) << std::endl;
    myfile<<"rayunit(3) mindistance plane(3)"<<std::endl;
	std::cout << "ray origin: " << ray_ori(0) << " " << ray_ori(1) << " " << ray_ori(2) << std::endl;

    int thres = 10;

    //char* path = "/home-nfs/zwu14/Documents/new-20140617-22/velodyne_points/data/";
	//char* path = argv[1]; //"/home-nfs/zwu14/Documents/raw_data/2011_09_26/2011_09_26_drive_0048_sync/velodyne_points/data/";
	char* path="./";
    std::cout << "Opening path: " << path << std::endl;
    DIR* VelodyneDir = opendir(path);
    if (VelodyneDir) {
        struct dirent* vFile;
        int VelInd = 0;
        while ((vFile = readdir(VelodyneDir)) != NULL) {
            if (strcmp(vFile->d_name, ".") == 0) continue;
            if (strcmp(vFile->d_name, "..") == 0) continue;

            //if (strcmp(vFile->d_name, "000200.txt") != 0) continue;
			if (strcmp(vFile->d_name, "000200.txt") != 0) continue;
            VelInd++; //if (VelInd != 201) continue;
            std::cout << "Opening laser file: " << vFile->d_name << std::endl;
            char *fullpath = (char*) malloc(strlen(path) + strlen(vFile->d_name) + 2);
            fullpath[0] = '\0';
            strcat(fullpath, path);
            strcat(fullpath, vFile->d_name);
            std::vector<vector<float> > laser_point = parseVelodynefile(fullpath);
            free(fullpath);
			std::cout << "parse velodyne file finished." << std::endl;

            //const char *segfile = "/home-nfs/zwu14/Documents/auto-eval/rayTra/test200/000200_seg.png";
			const char *segfile = "./000200_seg.png";//"/home-nfs/zwu14/Documents/auto-eval/rayTra/0000000008/0000000008L_seg.png";
            //const char *planefile = "/home-nfs/zwu14/Documents/auto-eval/rayTra/test200/000200_seg_planes.txt";
			const char *planefile = "./000200_seg_planes.txt";//"/home-nfs/zwu14/Documents/auto-eval/rayTra/0000000008/000000008_planes.txt";
            matrix<float> segment = loadSegfile(segfile);
			std::cout << "load seg file finished." << std::endl;

			//harry test

            std::vector<vector<float> > planes = parsePlanefile(planefile);
			std::cout << "parse plane file finished." << std::endl;

            std::vector<vector<float> > intersections(laser_point.size());
            std::vector<vector<float> > laser_in_camera(laser_point.size());
            //harry code
            std::vector<vector<float> > planes_save(laser_point.size());
            std::vector<float> ss_save(laser_point.size());
            std::vector<vector<float> > ray_units_save(laser_point.size());

            int m = segment.size1();
            int n = segment.size2();
            std::vector<vector<float> > bb = computeBoundingBox(segment);
			std::vector<std::vector<vector<float> > > segmentPixels = findSegment(segment);

            planes.resize(bb.size());

            int *distmap = (int*) malloc(sizeof (int)*m * n);
            memset(distmap, 0, sizeof (int)*m * n);
            for (int p = 0; p < laser_point.size(); p++) {
                if (p%10000==0) std::cout << "Processing laser point: " << p << std::endl;

                vector<float> intersection(3);
                intersection(0) = 0;
                intersection(1) = 0;
                intersection(2) = 0;
                vector<float> point = laser_point[p];
                if (point(0) == 0 && point(1) == 0 && point(2) == 0) {
                    laser_in_camera[p] = intersection;
                    intersections[p] = intersection;
                    ray_units_save[p]=intersection;
                    ss_save[p]=-1;
                    vector<float> h_plane(3);
                    h_plane(0)=0;
                    h_plane(1)=0;
                    h_plane(2)=0;
                    planes_save[p]=h_plane;
                    continue;
                }
                vector<float> X(4);
                X(0) = point(0);
                X(1) = point(1);
                X(2) = point(2);
                X(3) = 1;
                X = prod(M_v_c, X);
                vector<float> ray_unit(3);
                ray_unit(0) = X(0);
                ray_unit(1) = X(1);
                ray_unit(2) = X(2);
				//ray_unit = prod(R_rect, ray_unit);
                laser_in_camera[p] = ray_unit;
                ray_unit(0) = X(0) - ray_ori(0);
                ray_unit(1) = X(1) - ray_ori(1);
                ray_unit(2) = X(2) - ray_ori(2);
                ray_unit = ray_unit / norm_2(ray_unit); // normalize the ray unit vector

                float mindist = FLT_MAX,mindist2=FLT_MAX;
                int h_tmp=-1;
                for (int i = 0; i < planes.size(); i++) {
                    float u, v, s;

                    /* Finding intersection of ray and a plane */
                    vector<float> plane = planes[i];
					if (plane[4]==0) continue; // The plane is marked as in-valid
                    vector<float> plane_coef(3);
                    plane_coef(0) = plane(0);
                    plane_coef(1) = plane(1);
                    plane_coef(2) = plane(2);
                    float coef = inner_prod(plane_coef, ray_unit);
                    vector<float> X_in;
                    if (std::abs(coef) > EPS) {
                        s = (plane(3) + inner_prod(plane_coef, ray_ori)) / -coef;
                        X_in = ray_ori + s * ray_unit;
						if (X_in(2) < 0) { // ray-plane intersection fall at the back of the camera
							u = -1;
							v = -1;
							s = -1;
						}
						else {
                        	vector<float> U_in = prod(M_Proj, X_in);
							//U_in = prod(M_Proj, U_in);
                        	u = U_in(0) / U_in(2);
                        	v = U_in(1) / U_in(2);
						}
                    } else {
                        u = -1;
                        v = -1;
                        s = -1;
                    }

                    /* candidate-polling algorithm */
                    if (s < 0)
                        continue;
                    //harry code: crop the left and right boundary
                    if (u - 1 <= 150 || u > m-50)
                        continue;
                    if (v - 1 <= 0 || v > n)
                        continue;

                    //harry code: find the best intersection
                    double ss=(X(0)-X_in(0))*(X(0)-X_in(0))+(X(1)-X_in(1))*(X(1)-X_in(1))+(X(2)-X_in(2))*(X(2)-X_in(2));
                    //double ss=std::max(fabs(X(0)-X_in(0)),fabs(X(1)-X_in(1)));
                    //ss=std::max(ss,fabs(X(2)-X_in(2)));

                    if (segment(round(u) - 1, round(v) - 1) == i) // Case 1: Intersection point fall into the same segment region
                    {
                        if (ss < mindist2 && s > 0) { // finding the intersection with the minimal distance
                            mindist = s;
                            mindist2=ss;
                            intersection = ray_ori + s * ray_unit;
                            h_tmp=i;
                        }
                    }else {
                        vector<float> uv(2);
                        uv(0) = round(u);
                        uv(1) = round(v);
                        //int dis = findNearestSegment(uv, segment, i, distmap, thres); // The BFS method - computationally expensive
                        int dis = findNearestSegment_approx(uv, bb, i); // An Bounding-box approximate method
						//int dis = findNearestSegment(uv, segmentPixels, i);
						if (dis > thres)
                            continue;
                        else // Case 2: Intersection point fall within a certain distance to the segment region
                        {
                            if (mindist < FLT_MAX && ss < mindist2 && s > 0) { // finding the intersection with the minimal distance
                                mindist = s;
                                mindist2=ss;
                                intersection = ray_ori + s * ray_unit;
                            }
                        }
                    }


                } // end of loop for planes

                intersections[p] = intersection;
				if (intersection(0) == 0 && intersection(1) == 0 && intersection(2) == 0)
					laser_in_camera[p] = intersection;
					//harry code
                ray_units_save[p]=ray_unit;
                ss_save[p]=mindist;
                vector<float> h_plane(3);

                if(h_tmp!=-1)
                {

                    h_plane(0)=planes[h_tmp](0);
                    h_plane(1)=planes[h_tmp](1);
                    h_plane(2)=planes[h_tmp](2);
                }
                else
                {
                    h_plane(0)=0;
                    h_plane(1)=0;
                    h_plane(2)=0;
                }
                planes_save[p]=h_plane;
            } // end of loop for laser points
            free(distmap);

            /* Output the ray-tracing result */
            char *outputfilename = (char*) malloc(strlen(path) + strlen(vFile->d_name) + 2);
            outputfilename[0] = '\0';
            strcat(outputfilename, "raytrace_");
            strcat(outputfilename, vFile->d_name);
            std::ofstream outputfile(outputfilename);
            for (unsigned i = 0; i < intersections.size(); i++) {
                outputfile << intersections[i](0) << " " << intersections[i](1) << " " << intersections[i](2) << std::endl;
            }
            outputfile.close();
            outputfilename[0] = '\0';
            strcat(outputfilename, "lasercam_");
            strcat(outputfilename, vFile->d_name);
            outputfile.open(outputfilename);
            for (unsigned i = 0; i < laser_in_camera.size(); i++) {
                outputfile << laser_in_camera[i](0) << " " << laser_in_camera[i](1) << " " << laser_in_camera[i](2) << std::endl;
            }
            outputfile.close();
            free(outputfilename);

            //harry code
            std::cout<<ray_units_save.size()<<" "<<ss_save.size()<<" "<<planes_save.size()<<std::endl;
            for(int i=0;i<laser_in_camera.size();i++)
            {
                myfile<<laser_in_camera[i][0]<<" "<<laser_in_camera[i](1) << " " << laser_in_camera[i](2) << std::endl;
                myfile<< intersections[i](0) << " " << intersections[i](1) << " " << intersections[i](2) << std::endl;
                myfile<<ray_units_save[i](0)<<" "<<ray_units_save[i](1)<<" "<<ray_units_save[i](2)<<" "<<ss_save[i]<<" "<<planes_save[i](0)<<" "<<planes_save[i](1)<<" "<<planes_save[i](2)<<std::endl;
            }
            myfile.close();
        } // end of scanning velodyne folder
        closedir(VelodyneDir);
    } else
        std::cout << "Error open the Velodyne folder!" << std::endl;

    return 0;

}

int findNearestSegment(vector<float> &uv, std::vector<std::vector<vector<float> > >&segment, int segId) {
	float mindist = FLT_MAX;
	for (unsigned i=0; i<segment[segId].size(); i++) {
		float dist = norm_2(segment[segId][i] - uv);
		if (dist < mindist) mindist = dist;
	}
	return mindist;
}

std::vector<std::vector<vector<float> > > findSegment(matrix<float> &seg) {
	int maxR = 0;
    for (unsigned int u=0; u<seg.size1(); u++)
        for (unsigned int v=0; v<seg.size2(); v++)
            maxR = maxR > seg(u,v)? maxR:seg(u,v);

    std::vector<std::vector<vector<float> > > segment(maxR+1);
    for (unsigned int u=0; u<seg.size1(); u++)
        for (unsigned int v=0; v<seg.size2(); v++) {
			vector<float> pix(2);
			pix(0) = u, pix(1) = v;
            int segId = seg(u,v);
			segment[segId].push_back(pix);
        }

    return segment;

}


int findNearestSegment_approx(vector<float> &uv, std::vector<vector<float> > &bb, int segId) {

    int u = uv(0)-1, v = uv(1)-1;
    float umin = bb[segId](0), umax = bb[segId](1);
    float vmin = bb[segId](2), vmax = bb[segId](3);
    // Case 1: fall into the bounding box
    if (u >= umin && u <= umax && v >= vmin && u <= vmax)
        return 0;
    // Case 2: distance to the bounding box
    // Divide the space into 8 regions as shown below:
    //   1 |    2    |  3
    //  ---|---------|-----    vmax
    //   4 | segment |  5
    //  ---|---------|-----    vmin
    //   6 |    7    |  8
    //    umin     umax

    if (u < umin && v > vmax) // in region 1
        return std::sqrt((u-umin)*(u-umin)+(v-vmax)*(v-vmax));
    if (u > umax && v > vmax) // in region 3
        return std::sqrt((u-umax)*(u-umax)+(v-vmax)*(v-vmax));
    if (v > vmax) // in region 2
        return v - vmax;
    if (u < umin && v < vmin) // in region 6
        return std::sqrt((u-umin)*(u-umin)+(v-vmin)*(v-vmin));
    if (u < umin) // in region 4
        return umin - u;
    if (u > umax && v < vmin) // in region 8
        return std::sqrt((u-umax)*(u-umax)+(v-vmin)*(v-vmin));
    if (v < vmin) // in region 7
        return vmin - v;
    if (u > umax) // in region 5
        return u - umax;

    return INT_MAX;
}

std::vector<vector<float> > computeBoundingBox(matrix<float> &seg) {

    int maxR = 0;
    for (unsigned int u=0; u<seg.size1(); u++)
        for (unsigned int v=0; v<seg.size2(); v++)
            maxR = maxR > seg(u,v)? maxR:seg(u,v);
    std::cout << "maxR: " << maxR << std::endl;
    std::vector<vector<float> > bb(maxR+1);
    int m = seg.size1();
    int n = seg.size2();
    for (int i=0; i<maxR+1; i++){
        vector<float> newbb(4);
        newbb(0) = m;
        newbb(1) = 0;
        newbb(2) = n;
        newbb(3) = 0;
        bb[i] = newbb;
    }
    for (unsigned int u=0; u<seg.size1(); u++) {
        for (unsigned int v=0; v<seg.size2(); v++) {
            int segId = seg(u,v);
            bb[segId](0) = bb[segId](0)<=u ? bb[segId](0) : u;
            bb[segId](1) = bb[segId](1)>=u ? bb[segId](1) : u;
            bb[segId](2) = bb[segId](2)<=v ? bb[segId](2) : v;
            bb[segId](3) = bb[segId](3)>=v ? bb[segId](3) : v;
        }
    }

    return bb;
}

int findNearestSegment(vector<float> &uv, matrix<float> &seg, int segId, int *distmap, int thres) {
    int m = seg.size1();
    int n = seg.size2();

    int v = uv(0), u = uv(1);

    //int *distmap = (int*) malloc(sizeof (int)*m * n);
    memset(distmap, 0, sizeof (int)*m * n);
    //for (int i = 0; i < m * n; i++)
    //    distmap[i] = -1;
    distmap[(u - 1) * n + v - 1] = 0;
    std::queue<std::pair<int, int> > q;
    q.push(std::pair<int, int>(u - 1, v - 1));
    int dist = findNearest(q, segId, seg, distmap, m, n, thres);

    //free(distmap);
    return dist;
}

int findNearest(std::queue<std::pair<int, int> > &queue, int i, matrix<float> &seg, int *distmap, int m, int n, int thres) {

    int dist = INT_MAX;
    while (!queue.empty()) {
        std::pair<int, int> cur = queue.front();
        queue.pop();
        int u = cur.first, v = cur.second;
        if (distmap[u*n+v] == thres)
            return INT_MAX;

        if (u + 1 < m && distmap[(u + 1) * n + v] == 0) {
            distmap[(u + 1) * n + v] = distmap[u * n + v] + 1;
            if (seg(u + 1, v) == i) {
                dist = distmap[(u + 1) * n + v];
                break;
            }
            queue.push(std::pair<int, int>(u + 1, v));
        }
        if (u - 1 >= 0 && distmap[(u - 1) * n + v] == 0) {
            distmap[(u - 1) * n + v] = distmap[u * n + v] + 1;
            if (seg(u - 1, v) == i) {
                dist = distmap[(u - 1) * n + v];
                break;
            }
            queue.push(std::pair<int, int>(u - 1, v));
        }
        if (v + 1 < n && distmap[u * n + v + 1] == 0) {
            distmap[u * n + v + 1] = distmap[u * n + v] + 1;
            if (seg(u, v + 1) == i) {
                dist = distmap[u * n + v + 1];
                break;
            }
            queue.push(std::pair<int, int>(u, v + 1));
        }
        if (v - 1 >= 0 && distmap[u * n + v - 1] == 0) {
            distmap[u * n + v - 1] = distmap[u * n + v] + 1;
            if (seg(u, v - 1) == i) {
                dist = distmap[u * n + v - 1];
                break;
            }
            queue.push(std::pair<int, int>(u, v - 1));
        }
        if (u + 1 < m && v + 1 < n && distmap[(u + 1) * n + v + 1] == 0) {
            distmap[(u + 1) * n + v + 1] = distmap[u * n + v] + 1;
            if (seg(u + 1, v + 1) == i) {
                dist = distmap[(u + 1) * n + v + 1];
                break;
            }
            queue.push(std::pair<int, int>(u + 1, v + 1));
        }
        if (u + 1 < m && v - 1 >= 0 && distmap[(u + 1) * n + v - 1] == 0) {
            distmap[(u + 1) * n + v - 1] = distmap[u * n + v] + 1;
            if (seg(u + 1, v - 1) == i) {
                dist = distmap[(u + 1) * n + v - 1];
                break;
            }
            queue.push(std::pair<int, int>(u + 1, v - 1));
        }
        if (u - 1 >= 0 && v + 1 < n && distmap[(u - 1) * n + v + 1] == 0) {
            distmap[(u - 1) * n + v + 1] = distmap[u * n + v] + 1;
            if (seg(u - 1, v + 1) == i) {
                dist = distmap[(u - 1) * n + v + 1];
                break;
            }
            queue.push(std::pair<int, int>(u - 1, v + 1));
        }
        if (u - 1 >= 0 && v - 1 >= 0 && distmap[(u - 1) * n + v - 1] == 0) {
            distmap[(u - 1) * n + v - 1] = distmap[u * n + v] + 1;
            if (seg(u - 1, v - 1) == i) {
                dist = distmap[(u - 1) * n + v - 1];
                break;
            }
            queue.push(std::pair<int, int>(u - 1, v - 1));
        }
    }

    return dist;
}

matrix<float> loadSegfile(const char *segfile) {

    rev::Image<unsigned short> segImage;
    segImage = rev::read16bitImageFile(segfile);

    matrix<float> segment(segImage.width(), segImage.height());
    for (int u = 0; u < segImage.width(); u++)
        for (int v = 0; v < segImage.height(); v++)
            segment(u, v) = segImage(u, v);

    return segment;

}

std::vector<vector<float> > parsePlanefile(const char *planefile) {

    std::vector<vector<float> > planes;
    std::ifstream input(planefile);
    if (!input.is_open()) {
        std::cout << "Open file error: " << planefile << std::endl;
        return planes;
    }
    float tmp;
	char c;
    while (!input.eof()) {
        vector<float> plane(5);
        input >> tmp;
        input >> tmp;
        input >> tmp;
        input >> tmp;
        input >> tmp;
        input >> plane(0);
        input >> plane(1);
        input >> plane(2);
        input >> plane(3);
		input >> c;
		if (c == 'Y')
			plane(4)=1;
		else
			plane(4)=0;
        planes.push_back(plane);
    }
    return planes;
}

std::vector<vector<float> > parseVelodynefile(const char *Velodynefile) {

    std::vector<vector<float> > laser_points;
    std::ifstream input(Velodynefile);
    if (!input.is_open()) {
        std::cout << "Open file error: " << Velodynefile << std::endl;
        return laser_points;
    }

    float tmp;
    while (!input.eof()) {
        vector<float> point(6);
        input >> tmp;
        point(0) = tmp/100;
        input >> tmp;
        point(1) = tmp/100;
        input >> tmp;
        point(2) = tmp/100;
        input >> tmp;
        point(3) = tmp;
        input >> tmp;
        point(4) = tmp;
        input >> tmp;
        point(5) = tmp;
        laser_points.push_back(point);
    }
    return laser_points;
}


