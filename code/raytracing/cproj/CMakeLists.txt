#Project
cmake_minimum_required(VERSION 2.4)
project(castRay)


# Boost and its components
find_package( Boost REQUIRED )

if ( NOT Boost_FOUND )
  message(STATUS "This project requires the Boost library, and will not be compiled.")
  return()  
endif()


# Include directories
include_directories(include src/revlib/include src/revlib/external/libpng src/revlib/external/zlib src/common)

# Library directories
#link_directories(src/revlib)

# Flags
set(CMAKE_CXX_FLAGS_RELEASE "-Wall -O3 -msse4.2")
set(CMAKE_BUILD_TYPE Release)
#set(CMAKE_BUILD_TYPE Debug)

# Output directories
set(LIBRARY_OUTPUT_PATH src/revlib)
set(EXECUTABLE_OUTPUT_PATH bin)

# RevLib
file(GLOB REV_SRC_FILES "src/revlib/src/*.cpp" "src/revlib/external/libpng/*.c" "src/revlib/external/zlib/*.c")
add_library(revlib ${REV_SRC_FILES})

# CastRay
file(GLOB CASTRAY_SRC_FILES "src/*.cpp")
add_executable(castRay ${CASTRAY_SRC_FILES})
target_link_libraries(castRay revlib)
