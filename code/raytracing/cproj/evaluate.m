%%
% This matlab program evaluate the measurement error between 
% ray laser data (lasercam_xxxxxx.txt) and our ray casting model 
% (raytrace_xxxxx.txt). It measures the distance between 
% each corresponding points in the ray-direction.
% 
% Author: Zhixiang Wu, 07-22-2014

clear;

load lasercam_000200.txt;
load raytrace_000200.txt;

origin = [ 0.260236 -0.353415 -0.636309];

lasercam = zeros(size(lasercam_000200));
lasercam_unit = zeros(size(lasercam_000200));
lasercam_len = zeros(size(lasercam_000200,1),1);
raytrace = zeros(size(raytrace_000200));
for i=1:size(lasercam,1)
    if lasercam_000200(i,1)~=0 && lasercam_000200(i,1)~=0 && lasercam_000200(i,1)~=0
        lasercam(i,:) = lasercam_000200(i,:) - origin;
        lasercam_len(i) = sqrt(sum(lasercam(i,:).^2));
        lasercam_unit(i,:) = lasercam(i,:) ./ sqrt(sum(lasercam(i,:).^2));
        raytrace(i,:) = raytrace_000200(i,:) - origin;
    end
end

diff = lasercam - raytrace;
diff2 = zeros(size(raytrace_000200,1),1);
for i=1:size(lasercam,1)
    if lasercam(i,1)~=0 && lasercam(i,1)~=0 && lasercam(i,1)~=0
        diff2(i) = diff(i,1) ./ lasercam_unit(i,1);
    end
end

[H,X]=hist(diff2(diff2~=0),100);