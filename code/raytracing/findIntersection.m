function [u,v,s] = findIntersection(ray, plane, Proj)
% Input: 
%      ray - [x0,xt; y0,yt; z0,zt]
%      plane - [A,B,C,D] 
%      Proj - 3*3 Camera Projection matrix (intrinsic)
% Output:
%      u,v - intersection pixel
%      s - distance

coef = plane(1:3)*ray(:,2);
if (coef~=0)
    s = (plane(4) - plane(1:3) * ray(:,1)) / coef;
    X = ray(:,1) + s*ray(:,2);
    U = Proj * X;
    U = U / U(3);
    u = U(1);
    v = U(2);
else
    u = NaN;
    v = NaN;
    s = -1;
end

end
