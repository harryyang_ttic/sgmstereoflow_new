#include "mex.h"
#include <queue>

#define U_IN prhs[0]
#define SEG_IN prhs[1]
#define I_IN prhs[2]
#define D_OUT plhs[0]

int findNearest(std::queue<std::pair<int,int> > &queue, int i, const double *seg, int *distmap, int m, int n);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	if (nrhs != 3 || nlhs > 1)
		mexErrMsgTxt("Wrong number of parameters. dist=findNearestSegment(U,segment,i)");

	int n = mxGetM(SEG_IN);
	int m = mxGetN(SEG_IN);
    
	int segId = mxGetScalar(I_IN);
	const double *uv = mxGetPr(U_IN);
	const double *seg = mxGetPr(SEG_IN);
    
	int v = uv[0], u = uv[1];
	
	int *distmap = (int*)malloc(sizeof(int)*m*n);
	for (int i=0; i<m*n; i++)
		distmap[i] = -1;
	distmap[(u-1)*n+v-1] = 0;
	std::queue<std::pair<int,int> > q;
	q.push(std::pair<int,int>(u-1,v-1));
	int dist = findNearest(q, segId, seg, distmap, m, n);
    
	D_OUT = mxCreateDoubleScalar(dist);
	free(distmap);
	return;
}

int findNearest(std::queue<std::pair<int,int> > &queue, int i, const double *seg, int *distmap, int m, int n){
    
	int dist = INT_MAX;
	while (!queue.empty()) {
		std::pair<int,int> cur = queue.front();
		queue.pop();
		int u = cur.first, v = cur.second;
		
		if (u+1<m && distmap[(u+1)*n+v]==-1) {
			distmap[(u+1)*n+v]=distmap[u*n+v]+1;
			if (seg[(u+1)*n+v]==i) {
				dist = distmap[(u+1)*n+v];
				break;
			}
			queue.push(std::pair<int,int>(u+1,v));
		}
		if (u-1>=0 && distmap[(u-1)*n+v]==-1) {
			distmap[(u-1)*n+v]=distmap[u*n+v]+1;
			if (seg[(u-1)*n+v]==i) {
				dist = distmap[(u-1)*n+v];
				break;
			}
			queue.push(std::pair<int,int>(u-1,v));
		}
		if (v+1<n && distmap[u*n+v+1]==-1) {
			distmap[u*n+v+1]=distmap[u*n+v]+1;
			if (seg[u*n+v+1]==i) {
				dist = distmap[u*n+v+1];
				break;
			}
			queue.push(std::pair<int,int>(u,v+1));
		}
		if (v-1>=0 && distmap[u*n+v-1]==-1) {
			distmap[u*n+v-1]=distmap[u*n+v]+1;
			if (seg[u*n+v-1]==i) {
				dist = distmap[u*n+v-1];
				break;
			}
			queue.push(std::pair<int,int>(u,v-1));
		}
		if (u+1<m && v+1<n && distmap[(u+1)*n+v+1]==-1) {
			distmap[(u+1)*n+v+1]=distmap[u*n+v]+1;
			if (seg[(u+1)*n+v+1]==i) {
				dist = distmap[(u+1)*n+v+1];
				break;
			}
			queue.push(std::pair<int,int>(u+1,v+1));
		}
		if (u+1<m && v-1>=0 && distmap[(u+1)*n+v-1]==-1) {
			distmap[(u+1)*n+v-1]=distmap[u*n+v]+1;
			if (seg[(u+1)*n+v-1]==i) {
				dist = distmap[(u+1)*n+v-1];
				break;
			}
			queue.push(std::pair<int,int>(u+1,v-1));
		}
		if (u-1>=0 && v+1<n && distmap[(u-1)*n+v+1]==-1) {
			distmap[(u-1)*n+v+1]=distmap[u*n+v]+1;
			if (seg[(u-1)*n+v+1]==i) {
				dist = distmap[(u-1)*n+v+1];
				break;
			}
			queue.push(std::pair<int,int>(u-1,v+1));
		}
		if (u-1>=0 && v-1>=0 && distmap[(u-1)*n+v-1]==-1) {
			distmap[(u-1)*n+v-1]=distmap[u*n+v]+1;
			if (seg[(u-1)*n+v-1]==i) {
				dist = distmap[(u-1)*n+v-1];
				break;
			}
			queue.push(std::pair<int,int>(u-1,v-1));
		}
	}
	
	return dist;
}

