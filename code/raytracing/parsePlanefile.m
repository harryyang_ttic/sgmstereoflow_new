function planes = parsePlanefile(planefile)
raw = load(planefile);
planes = raw(:,[6,7,8,9]);
end