function M_v_to_c = getVelToCam()
% This function returns the velodyne to camera transformation matrix
%
% Zhixiang Wu
% Toyota Technological Institute Chicago
% 07-17-2014


% Projection matrix P = K*R*T 
P = [785.76 -1038.29 5.46148 -633.268;
	611.294 -18.4835 -1039.64 -872.053;
	0.999913 -0.00979284 -0.00878292 -1.06477];

[K,R,C,pp,pv] = decomposecamera(P);
M_a_to_cam = [R,C];
M_a_to_cam(4,4) = 1;

% transformation matrix from applanix(GPS) to velodyne
M_a_to_v = [1.00000 0.00070 -0.00177 0.41351;
            -0.00069 1.00000 0.00239 0.02507;
             0.00178 -0.00239 1.00000 0.09500;
             0 0 0 1];
M_v_to_a = inv(M_a_to_v);

M_v_to_c = M_v_to_a * M_a_to_cam;


%% velodyne to camera transformation provided by the KITTI dataset (can be used for reference)
% calib_velo_to_cam.txt
% R: 7.533745e-03 -9.999714e-01 -6.166020e-04 1.480249e-02 7.280733e-04 -9.998902e-01 9.998621e-01 7.523790e-03 1.480755e-02
% T: -4.069766e-03 -7.631618e-02 -2.717806e-01

% M = [7.533745e-03 -9.999714e-01 -6.166020e-04 -4.069766e-03;
%    1.480249e-02 7.280733e-04 -9.998902e-01 -7.631618e-02;
%    9.998621e-01 7.523790e-03 1.480755e-02 -2.717806e-01;
%    0 0 0 1];

end
