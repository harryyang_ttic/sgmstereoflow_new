function segment=loadSegfile(segfile)
input=imread(segfile);
hb = input/256;
lb = input-256*hb;
segment = 256*lb+hb;
end